package Scrabblematics;

public abstract class RelocatableActor extends Actor {

	private void relocate() {
		throw new UnsupportedOperationException();
	}

	abstract protected boolean isRelocatable();

	/**
	 * 
	 * @param x
	 * @param y
	 */
	abstract protected boolean isInValidArea(int x, int y);

	protected abstract boolean isValidMove();

	protected abstract void doAfterMove();

}