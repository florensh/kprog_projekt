package Scrabblematics;

public class Configuration {

	private int maxStonesOfPlayer;
	private String imageOfFreeField;
	private String fontOfStone;
	private String font;
	private String imageOfStone;
	private int worldDimensionXValue;
	private int worldDimensionYValue;
	private int cellSize;
	private String xmlStoneDataFile;
	private int boardDimensionXValue;
	private int boardDimensionYValue;
	private String playerStoneAreaImage;
	private String boardFrameImage;
	private String finishRoundButtonImage;

	public int getMaxStonesOfPlayer() {
		return this.maxStonesOfPlayer;
	}

	/**
	 * 
	 * @param maxStonesOfPlayer
	 */
	public void setMaxStonesOfPlayer(int maxStonesOfPlayer) {
		this.maxStonesOfPlayer = maxStonesOfPlayer;
	}

	public String getImageOfFreeField() {
		return this.imageOfFreeField;
	}

	/**
	 * 
	 * @param imageOfFreeField
	 */
	public void setImageOfFreeField(String imageOfFreeField) {
		this.imageOfFreeField = imageOfFreeField;
	}

	public String getFontOfStone() {
		return this.fontOfStone;
	}

	/**
	 * 
	 * @param fontOfStone
	 */
	public void setFontOfStone(String fontOfStone) {
		this.fontOfStone = fontOfStone;
	}

	public String getFont() {
		return this.font;
	}

	/**
	 * 
	 * @param font
	 */
	public void setFont(String font) {
		this.font = font;
	}

	public String getImageOfStone() {
		return this.imageOfStone;
	}

	/**
	 * 
	 * @param imageOfStone
	 */
	public void setImageOfStone(String imageOfStone) {
		this.imageOfStone = imageOfStone;
	}

	public int getWorldDimensionXValue() {
		return this.worldDimensionXValue;
	}

	/**
	 * 
	 * @param worldDimensionXValue
	 */
	public void setWorldDimensionXValue(int worldDimensionXValue) {
		this.worldDimensionXValue = worldDimensionXValue;
	}

	public int getWorldDimensionYValue() {
		return this.worldDimensionYValue;
	}

	/**
	 * 
	 * @param worldDimensionYValue
	 */
	public void setWorldDimensionYValue(int worldDimensionYValue) {
		this.worldDimensionYValue = worldDimensionYValue;
	}

	public int getCellSize() {
		return this.cellSize;
	}

	/**
	 * 
	 * @param cellSize
	 */
	public void setCellSize(int cellSize) {
		this.cellSize = cellSize;
	}

	public String getXmlStoneDataFile() {
		return this.xmlStoneDataFile;
	}

	/**
	 * 
	 * @param xmlStoneDataFile
	 */
	public void setXmlStoneDataFile(String xmlStoneDataFile) {
		this.xmlStoneDataFile = xmlStoneDataFile;
	}

	public void getBoardDimensionXValue() {
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param boardDimensionXValue
	 */
	public void setBoardDimensionXValue(int boardDimensionXValue) {
		this.boardDimensionXValue = boardDimensionXValue;
	}

	public void getBoardDimensionYValue() {
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param boardDimensionYValue
	 */
	public void setBoardDimensionYValue(int boardDimensionYValue) {
		this.boardDimensionYValue = boardDimensionYValue;
	}

	public String getPlayerStoneAreaImage() {
		return this.playerStoneAreaImage;
	}

	/**
	 * 
	 * @param playerStoneAreaImage
	 */
	public void setPlayerStoneAreaImage(String playerStoneAreaImage) {
		this.playerStoneAreaImage = playerStoneAreaImage;
	}

	public String getBoardFrameImage() {
		return this.boardFrameImage;
	}

	/**
	 * 
	 * @param boardFrameImage
	 */
	public void setBoardFrameImage(String boardFrameImage) {
		this.boardFrameImage = boardFrameImage;
	}

	public String getFinishRoundButtonImage() {
		return this.finishRoundButtonImage;
	}

	/**
	 * 
	 * @param finishRoundButtonImage
	 */
	public void setFinishRoundButtonImage(String finishRoundButtonImage) {
		this.finishRoundButtonImage = finishRoundButtonImage;
	}

}