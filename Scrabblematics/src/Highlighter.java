import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The class of "Highlighter".
 * 
 * @author (Florens Hueckstaedt) 
 * @version (11.12.2013)
 */
public class Highlighter extends Actor
{
    /**
     * Act - do whatever the Highlighter wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
       //we do nothing here!
    }    
}
