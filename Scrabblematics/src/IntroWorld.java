
import greenfoot.Greenfoot;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import logging.ScrabblematicsLogger;

/**
 * The class "IntroWorld" is the world for the game setup.
 * 
 * @author (Florian Wohlgemuth) 
 * @version (09.12.2013)
 */
public class IntroWorld extends ScrabblematicsWorld
{
	private MenuFrame dialog;
	private GameSaver saver;



	/**
	 * The act method is called by the Greenfoot 
	 * framework at each action step in the environment.
	 */
	public void act() {

		if(dialog==null){
			dialog = new MenuFrame();
			dialog.setVisible(true);

		}
		super.act();
	}

	private GameSaver getSaver(){
		if(saver ==null){
			
			try {
				//Dynamic creation of an object which implements GameSaver
				Class<GameSaver> sa = (Class<GameSaver>) Class.forName(getConfiguration().getDefaultGameSaver());
				saver =  sa.newInstance();
			} catch (Exception e) {
				ScrabblematicsLogger.log(e);
			}
		}
		return saver;
	}



	/**
	 * The class "MenuFrame". generates the GUI MenuFrame.
	 * 
	 * @author (Florian Wohlgemuth)
	 * @version (09.12.2013)
	 */
	class MenuFrame extends JFrame{

		JLabel text;
		JButton newGameButton;
		JButton continueButton;
		JTextField eingabe;
		JFrame theMenu;

		/**
		 * The constructor generates the GUI MenuFrame.
		 */
		public MenuFrame(){
			theMenu = this;
			setTitle ("Main Menu");
			setSize(500,400);
			setVisible(true);
			setLocationRelativeTo(null);
			setLayout(null);

			text = new JLabel("Welcome to Scrabblematic!");
			text.setBounds(120,35,500,20);
			Font schrift = (text.getFont().deriveFont(Font.BOLD + Font.ITALIC, 20));
			text.setFont(schrift);
			add(text);

			newGameButton = new JButton("New Game");
			newGameButton.setBounds(170,120,150,50);
			newGameButton.addActionListener(new NewGameListener());
			add(newGameButton);
			continueButton = new JButton("Continue");
			continueButton.setBounds(170,200,150,50);
			continueButton.setEnabled(true);
			continueButton.addActionListener(new LoadGameListener());
			add(continueButton);


		}

		/**
		 * The class "NewGameListener".
		 * 
		 * @author (Florian Wohlgemuth)
		 * @version (09.12.2013)
		 */
		private class NewGameListener implements ActionListener{

			/**
			 * The public method "actionPerformed" sets one GUI on true and the other on false.
			 * So the first one will be closed and the instead the second one will open.
			 */
			public void actionPerformed(ActionEvent arg0) {
				JFrame jf3 = new PlayerInputFrame();
				jf3.setVisible(true);
				theMenu.setVisible(false);




			}
		}


		/**
		 * The class "LoadGameListener".
		 * 
		 * @author (Florian Wohlgemuth)
		 * @version (09.12.2013)
		 */
		private class LoadGameListener implements ActionListener{

			/**
			 * The public method "actionPerformed" sets one GUI on true and the other on false.
			 * So the first one will be closed and the instead the second one will open.
			 */
			public void actionPerformed(ActionEvent arg0) {
				SaveGame sg = getSaver().load();
				Greenfoot.setWorld(new ScrabblematicsWorld(sg));

			}
		}
	}

}






/**
 * The class "PlayerInputFrame".
 * 
 * @author (Florian Wohlgemuth)
 * @version (09.12.2013)
 */
class PlayerInputFrame extends JFrame{
	
	JLabel text;
	JTextField eingabe1;
	JTextField eingabe2;
	JTextField eingabe3;
	JTextField eingabe4;
	JButton startButton;
	JFrame thePlayerInput;
	
	/**
	 *  The constructor generates the GUI PlayerInputFrame.
	 */
	public PlayerInputFrame(){
		thePlayerInput = this;
		setTitle("Player Input Menu");
		setSize(400,500);
		setVisible(false);
		setLocationRelativeTo(null);
		setLayout(null);
		
		text = new JLabel("Geben Sie Ihre Namen ein");
		text.setBounds(70,35,500,20);
		Font schrift = (text.getFont().deriveFont(Font.BOLD + Font.ITALIC, 20));
		text.setFont(schrift);
		add(text);
		
		eingabe1 = new JTextField();
		eingabe1.setBounds(70,90,150,25);
		add(eingabe1);
		
		eingabe2 = new JTextField();
		eingabe2.setBounds(70,140,150,25);
		add(eingabe2);
		
		eingabe3 = new JTextField();
		eingabe3.setBounds(70,190,150,25);
		add(eingabe3);
		
		eingabe4 = new JTextField();
		eingabe4.setBounds(70,240,150,25);
		add(eingabe4);
		
		startButton = new JButton("Start");
		startButton.setBounds(70,310,150,50);
		startButton.addActionListener(new StartListener());
		add(startButton);
		
		}
	
	private class StartListener implements ActionListener{

		/**
		 * The public method "actionPerformed" sets the GUI PlayerInputFrame on false and saves the 
		 * inputs for the names in the player list.
		 */
		public void actionPerformed(ActionEvent e) {
			List<Player> player = new ArrayList<Player>();
			if(!eingabe1.getText().isEmpty()){
				Player p1 = new Player(eingabe1.getText());
				player.add(p1);
			}
			
			if(!eingabe2.getText().isEmpty()){
				Player p2 = new Player(eingabe2.getText());
				player.add(p2);
			}
			
			if(!eingabe3.getText().isEmpty()){
				Player p3 = new Player(eingabe3.getText());
				player.add(p3);
			}
			
			if(!eingabe4.getText().isEmpty()){
				Player p4 = new Player(eingabe4.getText());
				player.add(p4);
			}
			Greenfoot.setWorld(new ScrabblematicsWorld(player));
			thePlayerInput.setVisible(false);
		}
	}
}