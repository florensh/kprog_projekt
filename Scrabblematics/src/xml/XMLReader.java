package xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Class which reads from a XML file the child nodes of the root node and
 * facilitates the access to the these nodes
 * 
 * @author Catalin Epure
 * @author Ulrike Jaeger
 * @author Peter Schiffmann
 * @version 0.4 2013-12-08 by P.S.
 */
public class XMLReader {

	/**
	 * A special exception class for the <code>XMLReader</code>.
	 * 
	 * @author Catalin Epure
	 */
	public class XMLReaderException extends Exception {

		/** serial version UID */
		private static final long serialVersionUID = 1L;

		/**
		 * Creates a new <code>XMLReaderException</code> with a specified
		 * message.
		 * 
		 * @param message
		 *            the message which describes the reason for this
		 *            exception's occurance
		 */
		public XMLReaderException(String message) {
			super(message);
		}

		/**
		 * Creates a new <code>XMLReaderException</code> with a specified
		 * message and cause.
		 * 
		 * @param message
		 *            the message which describes the reason for this
		 *            exception's occurance
		 * @param cause
		 *            detailed exception causing this exception
		 */
		public XMLReaderException(String message, Throwable cause) {
			super(message, cause);
		}
	} // end of XMLException class

	/** DOM representation of the XML file of this <code>XMLReader</code> */
	private org.w3c.dom.Document xmlDocument;

	/** All nodes contained in the XML file */
	private Vector<Node> xmlNodes;

	/**
	 * Creates a new <code>XMLReader</code> with a specified path and gets the
	 * nodes from the file.
	 * 
	 * @param path
	 *            path for loading the xml file
	 * @throws XMLReaderException
	 *             if any of these exceptions:
	 *             <ul>
	 *             <li><code>NullPointerException</code> - <code>path</code> is
	 *             null</li>
	 *             <li><code>ParserConfigurationException</code> - internal
	 *             <code>DocumentBuilder</code> could not be created</li>
	 *             <li><code>SAXException</code> - error while parsing the XML
	 *             file</li>
	 *             <li><code>FileNotFoundException</code> - found no file under
	 *             <code>path</code></li>
	 *             </ul>
	 *             occurs while creating the <code>XMLReader</code>. The
	 *             detailed causing exception can be found via this exception's
	 *             <code>getCause()</code> method.
	 */
	public XMLReader(String path) throws XMLReaderException {
		try { // tries to create a DOM document
				// create document builder
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			// parse to XML document
			this.xmlDocument = docBuilder.parse(new File(path));
		} catch (NullPointerException npE) {
			// throw XMLReaderException if the path is null
			throw new XMLReaderException("Specified path is NULL!", npE);
		} catch (ParserConfigurationException pcE) {
			// convert ParserConfigurationException to XMLReaderException
			throw new XMLReaderException(
					"Document builder for XML format could not be created!",
					pcE);
		} catch (SAXException saxE) {
			// convert SAXException to XMLReaderException
			throw new XMLReaderException(
					"Document doesn't match the XML format!", saxE);
		} catch (FileNotFoundException fnfE) {
			// convert FileNotFoundException to XMLReaderException
			throw new XMLReaderException("File not found: " + path, fnfE);
		} catch (IOException ioE) {
			// convert IOException to XMLReaderException
			throw new XMLReaderException("An IO error occured!", ioE);
		}
		// create a list of nodes
		this.xmlNodes = new Vector<Node>();
		// get nodes of the read XML-file
		getNodes();
	}

	/**
	 * Returns the text contents of all nodes in the XML file with a specified
	 * tag name.
	 * 
	 * @param tagName
	 *            tag name of the wanted nodes
	 * @return a list of the text contents of all nodes whose tag name equals to
	 *         <code>tagName</code>. If there are no such nodes, the list is
	 *         empty. If a node has no text content, the representing string is
	 *         empty.
	 */
	private Vector<String> getContentsOfTagName(String tagName) {
		// vector for text contents
		Vector<String> resultList = new Vector<String>();
		// get elements/nodes with the specified tag name
		NodeList resultNodes = this.xmlDocument.getElementsByTagName(tagName);
		// get the number of resulting elements/nodes
		int resultingNodesNumber = resultNodes.getLength();
		// copy all elements/nodes..
		for (int index = 0; index < resultingNodesNumber; index++) {
			// ..from the node list to the result list
			resultList.add(resultNodes.item(index).getTextContent());
		}
		return resultList;
	}

	/**
	 * Gets all those nodes from the XML file that are of elemental type (
	 * {@link Node#ELEMENT_NODE}) - so no tags including blank spaces etc.) and
	 * lists them in {@link XMLReader#xmlNodes}.
	 */
	private void getNodes() {
		// get all nodes
		NodeList nodes = this.xmlDocument.getFirstChild().getChildNodes();
		// Iterate over the nodes
		for (int i = 0; i < nodes.getLength(); i++) {
			// get single node
			Node node = nodes.item(i);
			// if node is of elemental type..
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				// .. add to class-internal node list
				this.xmlNodes.add(node);
			}
		}
	}

	/**
	 * Returns a list of all elemental nodes of the XML file of this
	 * <code>XMLReader</code>.
	 * 
	 * @return list of all elemental nodes
	 */
	public final Vector<Node> getXmlNodes() {
		return this.xmlNodes;
	}

	/**
	 * Returns the number of elemental nodes contained by the XML file.
	 * 
	 * @return the number of elemental nodes
	 */
	public final int numberOfNodes() {
		return this.xmlNodes.size();
	}

	/**
	 * Reads the nodes from the XML file with the following tags.
	 * 
	 * @param tagName
	 *            the name of the wanted tag from the XML file
	 * @return <code>Hashtable</code> containing the results for the
	 *         <code>tags</code> as key values. For example the first component
	 *         of the vector gotten by the key string <code>"name"</code> is
	 *         content of the first node with the tag name <code>"name"</code>
	 *         in the XML file. If a vector component is empty, the node at the
	 *         index of this component has no content.
	 */
	public Vector<String> readByTag(String tagName) {
		return getContentsOfTagName(tagName);
	}

	/**
	 * Reads all the nodes with a specified tag names from the XML file.
	 * 
	 * @param tagNames
	 *            the names of the wanted tags from the XML file
	 * @return a <code>Hashtable</code> containing the results for the
	 *         <code>tags</code> as keys and the text contents of the
	 *         corresponding nodes as value.<br>
	 *         For example the first component of the vector gotten by the key
	 *         string <code>"name"</code> is the content of the first node with
	 *         the tag name <code>"name"</code> in the XML file. If a vector
	 *         component is empty, the node at the index of this component has
	 *         no content. If the whole vector is empty, there are nodes with
	 *         the specified key tag name.
	 * @throws XMLReaderException
	 *             if <code>tags</code> contains duplicates.
	 */
	public Hashtable<String, Vector<String>> readByTags(String[] tagNames)
			throws XMLReaderException {
		// check tags for duplicates if more than 1 tag
		if (tagNames.length > 1) {
			// first comparative string iterates from first string to the one
			// before last string in tag names
			for (int firstIndex = 0; firstIndex < tagNames.length - 1; firstIndex++) {
				// second comparative string iterates from string after first
				// comparative string to last string in tag names
				for (int secondIndex = firstIndex + 1; secondIndex < tagNames.length; secondIndex++) {
					// throw exception if two tags are equal
					if (tagNames[firstIndex].equals(tagNames[secondIndex])) {
						throw new XMLReaderException(
								"Duplicate tag name to read: "
										+ tagNames[firstIndex]);
					}
				}
			}
		}// check tags finished

		// build resulting hash table
		Hashtable<String, Vector<String>> results = new Hashtable<String, Vector<String>>();
		// for all tags
		for (String tagName : tagNames) {
			// put text content for key tagName in the hashtable..
			results.put(tagName, getContentsOfTagName(tagName));
		}
		return results;
	}

}