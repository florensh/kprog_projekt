package xml;




class TagNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;
	private String message;
	
	public final String getMessage() {
		return message;
	}

	public TagNotFoundException(String message) {
		super(message);
		this.message = message;
	}
}
