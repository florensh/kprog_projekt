package xml;

import java.io.File;
import java.util.Hashtable;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Class used to create a new XML file
 * Hash tables will be used as node information to be added 
 * in the XML file 
 * @author Catalin Epure
 *
 */


public class XMLWriter {
	private org.w3c.dom.Document xmlDocument; //DOM representation of the XML file
	private Element root; // rootElement of the XML file
	private File file; // file used for making the connection between XML and the path
	private String elementsName; // the Elements' tag name to be added; 
   								// for example for "producers" will be "producer"
	
	/**
	 * creates a new instance of the class with an empty DOM document
	 * @param path to the XML file
	 * @param rootElement of the XML file
	 * @param elementsName the tag's name of the Elements to be contained
	 */
	public XMLWriter(String path, String rootElement, String elementsName) {
		DocumentBuilder builder;
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			xmlDocument = builder.newDocument();//creates an empty DOM document
			file = new File(path);//assign the path to the XML file
			root = xmlDocument.createElement(rootElement); //creates a root node element 
			this.elementsName = elementsName;//assigns the name of the Elements' tag name
		} catch (ParserConfigurationException e) {
			
		}
	}
		

	/**
	 * writes a new node in the XML file which comes as a hash table 
	 * @param info the hash table containing the information to be added
	 */
	public void write(Hashtable<String, String> info) {
		
		Node user = xmlDocument.appendChild(xmlDocument.createElement(elementsName)); //creates a new Node
																					//to be added to the file
		for (String key : info.keySet()) { //for all keys in the hashtable
			Element e = xmlDocument.createElementNS(null, key);//adds a new Element with the tag @key
			Node n= xmlDocument.createTextNode(info.get(key));//creates the element's information
			e.appendChild(n);//the information is a child of the new element node
			user.appendChild(e);//the new element is a child of the new node created
		}
		
		root.appendChild(user);//the new nod created is a child of the root element
		
	}
	
	/**
	 * finalizes the procedure of writing
	 */
	public void close() {
		xmlDocument.appendChild(root);//the rootelement is added to the DOM document
		Source source = new DOMSource(xmlDocument);//prepares the source to be written
		Result result = new StreamResult(file);//prepares the stream where to be written
		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.transform(source, result);//transformation of the DOM document into a XML one
		} catch (TransformerConfigurationException e) {
			
		} catch (TransformerFactoryConfigurationError e) {
			
		} catch (TransformerException e) {
			
		}
		System.out.println("successfully written!!");
	}
}
