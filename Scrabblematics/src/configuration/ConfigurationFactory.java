package configuration;

import java.util.Hashtable;
import java.util.Vector;

import logging.ScrabblematicsLogger;
import xml.XMLReader;
import xml.XMLReader.XMLReaderException;

/** 
 * Class to create the world with an XML-file, if the XML-file doesn't exist 
 * the world will be setted with default values.
 * 
 * @author (Patrick Wohlgemuth)
 * @version (09.12.2013)
 *
 */
public class ConfigurationFactory {
	
	/**
	 * The public method "Configuration" works with reading an XML-file and saved the values into a 
	 * hashtable (config) and retuned the hashtable to the class "configuration".
	 *
	 * If the XML-file doesn't exist the method (fillWithDefaultValues()) 
	 * will be called an setted the hashtable wir default values.
	 * 
	 *@return conifg(hashtable) with XML-values or default-Values. 
	 */
	public static Configuration createConfiguration(){
		
		Configuration config = new Configuration();
		XMLReader readerConfig;
		
		
		try {
			readerConfig = new XMLReader("xmldata/configuration.xml");
			
			String[] tags = {"font", "imageOfFreeField", "fontOfStone", "maxStonesOfPlayer", "cellSize", 
							"worldDimensionXValue", "worldDimensionYValue", "xmlStoneDataFile", "boardDimensionXValue",
							"boardDimensionYValue","imageOfStone","playerStoneAreaImage","boardFrameImage",
							"finishRoundButtonImage","numberOfOperationStones","numberOfOperandStones","numberOfEqualSignStones",
							"saveButtonImage", "safeGameFile","defaultEqualitySign", "defaultMinusSign", "defaultPlusSign","defaultGameSaver"};
		
			Hashtable<String,Vector<String>> configTable = readerConfig.readByTags(tags);
			
			String font = configTable.get("font").get(0);
			String bereinigtFont = font.trim();
			config.setFont(bereinigtFont);
			
			String imageOfFreeField = configTable.get("imageOfFreeField").get(0);
			String bereinigtImageOfFreeField = imageOfFreeField.trim();
			config.setImageOfFreeField(bereinigtImageOfFreeField);
			
			String fontOfStone = configTable.get("fontOfStone").get(0);
			String bereinigtFontOfStone = fontOfStone.trim();
			config.setFontOfStone(bereinigtFontOfStone);
			
			String maxStonesOfPlayer = configTable.get("maxStonesOfPlayer").get(0);
			String bereinigtMaxStonesOfPlayer = maxStonesOfPlayer.trim();
			config.setMaxStonesOfPlayer(Integer.parseInt(bereinigtMaxStonesOfPlayer));
		
			String cellSize = configTable.get("cellSize").get(0);
			String bereinigtCellSize = cellSize.trim();
			config.setCellSize(Integer.parseInt(bereinigtCellSize));
			
			String worldDimensionXValue = configTable.get("worldDimensionXValue").get(0);
			String bereinigtWorldDimensionXValue = worldDimensionXValue.trim();
			config.setWorldDimensionXValue(Integer.parseInt(bereinigtWorldDimensionXValue));
			
			String boardDimensionXValue = configTable.get("boardDimensionXValue").get(0);
			String bereinigtboardDimensionXValue = boardDimensionXValue.trim();
			config.setBoardDimensionXValue(Integer.parseInt(bereinigtboardDimensionXValue));
			
			String boardDimensionYValue = configTable.get("boardDimensionYValue").get(0);
			String bereinigtboardDimensionYValue = boardDimensionYValue.trim();
			config.setBoardDimensionYValue(Integer.parseInt(bereinigtboardDimensionYValue));
			
			String worldDimensionYValue = configTable.get("worldDimensionYValue").get(0);
			String bereinigtWorldDimensionYValue = worldDimensionYValue.trim();
			config.setWorldDimensionYValue(Integer.parseInt(bereinigtWorldDimensionYValue));
			
			String xmlStoneDataFile = configTable.get("xmlStoneDataFile").get(0);
			String bereinigtXmlStoneDataFile = xmlStoneDataFile.trim();
			config.setXmlStoneDataFile(bereinigtXmlStoneDataFile);
			
			String imageOfStone = configTable.get("imageOfStone").get(0);
			String bereinigtImageOfStone = imageOfStone.trim();
			config.setImageOfStone(bereinigtImageOfStone);
			
			String playerStoneAreaImage = configTable.get("playerStoneAreaImage").get(0);
			String bereinigtPlayerStoneAreaImage = playerStoneAreaImage.trim();
			config.setPlayerStoneAreaImage(bereinigtPlayerStoneAreaImage);
			
			String boardFrameImage = configTable.get("boardFrameImage").get(0);
			String bereinigtBoardFrameImage = boardFrameImage.trim();
			config.setBoardFrameImage(bereinigtBoardFrameImage);
			
			String finishRoundButtonImage = configTable.get("finishRoundButtonImage").get(0);
			String bereinigtFinishRoundButtonImage = finishRoundButtonImage.trim();
			config.setFinishRoundButtonImage(bereinigtFinishRoundButtonImage);
			
			String numberOfOperationStones = configTable.get("numberOfOperationStones").get(0);
			String bereinigtNumberOfOperationStones = numberOfOperationStones.trim();
			config.setNumberOfOperationStones(Integer.parseInt(bereinigtNumberOfOperationStones));
			
			String numberOfOperandStones = configTable.get("numberOfOperandStones").get(0);
			String bereinigtNumberOfOperandStones = numberOfOperandStones.trim();
			config.setNumberOfOperandStones(Integer.parseInt(bereinigtNumberOfOperandStones));
			
			String numberOfEqualSignStones = configTable.get("numberOfEqualSignStones").get(0);
			String bereinigtNumberOfEqualSignStones = numberOfEqualSignStones.trim();
			config.setNumberOfEqualSignStones(Integer.parseInt(bereinigtNumberOfEqualSignStones));
			
			String saveButtonImage = configTable.get("saveButtonImage").get(0);
			String bereinigtSaveButtonImage = saveButtonImage.trim();
			config.setSaveButtonImage(bereinigtSaveButtonImage);
			
			String safeGameFile = configTable.get("safeGameFile").get(0);
			String bereinigtSafeGameFile = safeGameFile.trim();
			config.setSaveGameFile(bereinigtSafeGameFile);
			
			String defaultEqualitySign = configTable.get("defaultEqualitySign").get(0);
			String bereinigtDefaultEqualitySign = defaultEqualitySign.trim();
			config.setDefaultEqualitySign(bereinigtDefaultEqualitySign);
			
			String defaultPlusSign = configTable.get("defaultPlusSign").get(0);
			String bereinigtDefaultPlusSign = defaultPlusSign.trim();
			config.setDefaultPlusSign(bereinigtDefaultPlusSign);
			
			String defaultMinusSign = configTable.get("defaultMinusSign").get(0);
			String bereinigtDefaultMinusSign = defaultMinusSign.trim();
			config.setDefaultMinusSign(bereinigtDefaultMinusSign);
			
			
			String defaultGameSaver = configTable.get("defaultGameSaver").get(0);
			String bereinigtDefaultGameSaver = defaultGameSaver.trim();
			config.setDefaultGameSaver(bereinigtDefaultGameSaver);
			
			

		} 	catch (Exception e) {
				fillWithDefault(config);
				ScrabblematicsLogger.log(e);
			}
		
		return config;
		
	}
	/**
	 * Method to fill default values to config.
	 * @param config
	 */
	private static void fillWithDefault(Configuration config){
		config.setFont(null);
		config.setImageOfFreeField("cell.jpg");
		config.setImageOfStone("stone.png");
		config.setPlayerStoneAreaImage("playerAreaCell.jpg");
		config.setFontOfStone(null);
		config.setMaxStonesOfPlayer(11);
		config.setCellSize(40);
		config.setWorldDimensionXValue(17);
		config.setWorldDimensionYValue(19);
		config.setXmlStoneDataFile("xmldata/stones.xml");
		config.setBoardDimensionXValue(15);
		config.setBoardDimensionYValue(15);
		config.setNumberOfOperationStones(3);
		config.setNumberOfOperandStones(5);
		config.setNumberOfEqualSignStones(3);
		config.setSaveButtonImage("SaveButton.png");
		config.setSaveGameFile("savegame.ser");
		config.setDefaultEqualitySign("=");
		config.setDefaultMinusSign("-");
		config.setDefaultPlusSign("+");
		config.setBoardFrameImage("emptyCell.jpg");
		config.setFinishRoundButtonImage("Thumb.png");
		config.setDefaultGameSaver("ByteStreamGameSaver");
		
	}
}	
	


