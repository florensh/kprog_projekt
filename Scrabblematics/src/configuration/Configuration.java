package configuration;
/**
 * 
 * 
 * @author Patrick Wohlgemuth
 * @author Florian Wohlgemuth
 * @author Florenz Hueckstaedt
 * @author Mustafa Uezuemcue
 * 
 * @version 12.12.2013
 * 
 * 
 * The configuration class of the game
 *
 */

public class Configuration {

	private int maxStonesOfPlayer;
	private String imageOfFreeField;
	private String fontOfStone;
	private String font;
	private String imageOfStone;
	private int worldDimensionXValue;
	private int worldDimensionYValue;
	private int cellSize;
	private String xmlStoneDataFile;
	private int boardDimensionXValue;
	private int boardDimensionYValue;
	private String playerStoneAreaImage;
	private String boardFrameImage;
	private String finishRoundButtonImage;
	private int numberOfOperationStones;
	private int numberOfOperandStones;
	private int numberOfEqualSignStones;
	private String saveButtonImage;
	private String saveGameFile;
	private String defaultEqualitySign;
	private String defaultPlusSign;
	private String defaultMinusSign;
	private String defaultGameSaver;
	
	
	/**
	 * getter of defaultGameSaver
	 * @return defaultGameSaver
	 */
	public String getDefaultGameSaver() {
		return defaultGameSaver;
	}


	/**
	 * setter of defaultGameSaver
	 * @param defaultGameSaver
	 */
	public void setDefaultGameSaver(String defaultGameSaver) {
		this.defaultGameSaver = defaultGameSaver;
	}


	/**
	 * getter of defaultPlusSign
	 * @return defaultPlusSign
	 */
	public String getDefaultPlusSign() {
		return defaultPlusSign;
	}


	/**
	 * setter of defaultPlusSign
	 * @param defaultPlusSign
	 */
	public void setDefaultPlusSign(String defaultPlusSign) {
		this.defaultPlusSign = defaultPlusSign;
	}

	

	/**
	 * getter of defaultMinusSign
	 * @return defaultMinusSign
	 */
	public String getDefaultMinusSign() {
		return defaultMinusSign;
	}


	/**
	 * setter of defaultMinusSign
	 * @param defaultMinusSign
	 */
	public void setDefaultMinusSign(String defaultMinusSign) {
		this.defaultMinusSign = defaultMinusSign;
	}


	/**
	 * getter for defaultEqualitySign
	 * @return defaultEqualitySign
	 */
	public String getDefaultEqualitySign() {
		return defaultEqualitySign;
	}
	
	
	/**
	 * setter for defaultEqualitySign
	 * @param defaultEqualitySign
	 */
	public void setDefaultEqualitySign(String defaultEqualitySign) {
		this.defaultEqualitySign = defaultEqualitySign;
	}
	
	
	/**
	 * getter for saveGameFile
	 * @return saveGameFile
	 */
	public String getSaveGameFile() {
		return saveGameFile;
	}
	
	
	/**
	 * setter for saveGameFile
	 * @param saveGameFile
	 */
	public void setSaveGameFile(String saveGameFile) {
		this.saveGameFile = saveGameFile;
	}
	
	
	/**
	 * getter
	 *@return the maximum of stones of player
	 */
	public int getMaxStonesOfPlayer(){
		return this.maxStonesOfPlayer;
	}
	
	
	/**
	 * set the maximum of Stones of one player from a xml-file or a default value
	 * @param maxStonesOfPlayer
	 */
	public void setMaxStonesOfPlayer(int maxStonesOfPlayer){
		this.maxStonesOfPlayer = maxStonesOfPlayer;
	}
	
	
	
	/**
	 * getter for the image of a free field
	 * @return the "imageOfFreeField"
	 */
	public String getImageOfFreeField(){
		return this.imageOfFreeField;
	}
	
	
	/**
	 * set the image of free field from a xml-file or a default value
	 * @param imageOfFreeField
	 */
	public void setImageOfFreeField(String imageOfFreeField){
		this.imageOfFreeField = imageOfFreeField;
	}

	
	
	/**
	 * getter for the font of stones
	 * @return the "fontOfStone"
	 */
	public String getFontOfStone() {
		return this.fontOfStone;
	}
	/**
	 * set the font of a stone from a xml-file or a default value
	 * @param fontOfStone
	 */
	public void setFontOfStone(String fontOfStone){
		this.fontOfStone = fontOfStone;
	}
	
	
	
	/**
	 * getter for the game font
	 * @return the "font"
	 */
	public String getFont(){
		return this.font;
	}
	/**
	 * set the font from a xml-file or a default value
	 * @param font
	 */
	public void setFont(String font){
		this.font = font;
	}

	
	
	/**
	 * getter for the image of stones
	 * @return the "imageOfStone"
	 */
	public String getImageOfStone(){
		return this.imageOfStone;
	}
	/**
	 * set the image of a stone from a xml-file or a default value
	 * @param imageOfStone
	 */
	public void setImageOfStone(String imageOfStone){
		this.imageOfStone = imageOfStone;
	}
	
	
	/**
	 * getter for the x-value of the hole world 
	 * @return the "worldDimensionXValue"
	 */
	public int getWorldDimensionXValue(){
		return this.worldDimensionXValue;
	}
	/**
	 * set the world dimension x-value from a xml-file or a default value
	 * @param worldDimensionXValue
	 */
	public void setWorldDimensionXValue(int worldDimensionXValue){
		this.worldDimensionXValue = worldDimensionXValue;
	}
	
	
	
	/**
	 * getter for the y-value of the hole world 
	 * @return the "worldDimensionYValue"
	 */
	public int getWorldDimensionYValue(){
		return this.worldDimensionYValue;
	}
	
	
	
	/**
	 * set the world dimension x-value from a xml-file or a default value
	 * @param worldDimensionYValue
	 */
	public void setWorldDimensionYValue(int worldDimensionYValue){
		this.worldDimensionYValue = worldDimensionYValue;
	}
	
	
	
	/**
	 * getter for the cellsize
	 * @return the "cellSize"
	 */
	public int getCellSize(){
		return this.cellSize;
	}
	/**
	 * set the cell size from a xml-file or a default value
	 * @param cellSize
	 */
	public void setCellSize(int cellSize){
		this.cellSize = cellSize;
	}

	
	
	/**
	 * getter for the location of the Xml-file for stones
	 * @return the "XmlStoneDataFile"
	 */
	public String getXmlStoneDataFile(){
		return this.xmlStoneDataFile;
	}
	/**
	 * set the xml-file location of a stone from a xml-file or a default value
	 * @param xmlStoneDataFile
	 */
	public void setXmlStoneDataFile(String xmlStoneDataFile){
		this.xmlStoneDataFile = xmlStoneDataFile;
	}

	
	
	/**
	 *	getter for the x-value of the board
	 * @return the "boardDimensionXValue" 
	 */
	public int getBoardDimensionXValue(){
		return this.boardDimensionXValue;
	}
	/**
	 * set the board dimension x-value from a xml-file or a default value
	 * @param boardDimensionXValue
	 */
	public void setBoardDimensionXValue(int boardDimensionXValue){
		this.boardDimensionXValue = boardDimensionXValue;
	}

	
	
	/**
	 * getter for the y-value of the board
	 * @return the "boardDimensionYValue" 
	 */
	public int getBoardDimensionYValue(){
		return this.boardDimensionYValue;
	}
	/**
	 * set the board dimension y-value from a xml-file or a default value
	 * @param boardDimensionYValue
	 */
	public void setBoardDimensionYValue(int boardDimensionYValue){
		this.boardDimensionYValue = boardDimensionYValue;
	}
	
	
	
	/**
	 * getter for the image of stones witch you get at the start of the game
	 * @return the "playerStoneAreaImage"
	 */
	public String getPlayerStoneAreaImage(){
		return this.playerStoneAreaImage;
	}
	/**
	 * set the player stone area image from a xml-file or a default value
	 * @param playerStoneAreaImage
	 */
	public void setPlayerStoneAreaImage(String playerStoneAreaImage) {
		this.playerStoneAreaImage = playerStoneAreaImage;
	}

	
	
	/**
	 * getter for the image around the "playerStoneAreaImage"
	 * @return the  "boardFrameImage"
	 */
	public String getBoardFrameImage() {
		return this.boardFrameImage;
	}
	/**
	 * set the board frame image from a xml-file or a default value
	 * @param boardFrameImage
	 */
	public void setBoardFrameImage(String boardFrameImage) {
		this.boardFrameImage = boardFrameImage;
	}

	
	
	/**
	 * getter for the (thump up image)
	 * @return the "finishRoundButtonImage" (thump up image)
	 */
	public String getFinishRoundButtonImage() {
		return this.finishRoundButtonImage;
	}
	/**
	 * set the finish round button image from a xml-file or a default value
	 * @param finishRoundButtonImage
	 */
	public void setFinishRoundButtonImage(String finishRoundButtonImage) {
		this.finishRoundButtonImage = finishRoundButtonImage;
	}

	
	
	/**
	 * getter the number of operation stones
	 * @return the "NumberOfOperationStones"
	 */
	public int getNumberOfOperationStones() {
		return this.numberOfOperationStones;
	}
	/**
	 * set the number of operation stones from a xml-file or a default value
	 * @param numberOfOperationStones
	 */
	public void setNumberOfOperationStones(int numberOfOperationStones) {
		this.numberOfOperationStones = numberOfOperationStones;
	}
	

	
	/**
	 * getter for the numer of operand stones
	 * @return the "numberOfOperandStones"
	 */
	public int getNumberOfOperandStones() {
		return this.numberOfOperandStones;
	}
	/**
	 * set the number of operand stones from a xml-file or a default value
	 * @param numberOfOperandStones
	 */
	public void setNumberOfOperandStones(int numberOfOperandStones) {
		this.numberOfOperandStones = numberOfOperandStones;
	}
	

	
	/**
	 * getter for the numer of equals stones
	 * @return the "NumberOfEqualSignStones"
	 */
	public int getNumberOfEqualSignStones() {
		return this.numberOfEqualSignStones;
	}
	/**
	 * set the number of equals stones from a xml-file or a default value
	 * @param numberOfEqualSignStones
	 */
	public void setNumberOfEqualSignStones(int numberOfEqualSignStones) {
		this.numberOfEqualSignStones = numberOfEqualSignStones;
	}
	
	
	
	
	/**
	 * getter for the save button image
	 * @return the "SaveButtonImage"
	 */
	public String getSaveButtonImage() {
		return this.saveButtonImage;
	}
	/**
	 * set the save button image from a xml-file or default image
	 * @param "saveButtonImage"
	 */
	public void setSaveButtonImage(String saveButtonImage) {
		this.saveButtonImage = saveButtonImage;
	}
}