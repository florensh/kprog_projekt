import java.util.ArrayList;
import java.util.List;

/**
 * The referee contains logic to validate certain moves of stones.
 * @author Florens Hueckstaedt
 *
 */
public final class Referee {
	
	private static Referee theReferee = new Referee();
	private List<Highlighter> currentValidCells = new ArrayList<Highlighter>();
	

	public static Referee createReferee(){
		return theReferee;
	}
	
	
	//constructor is private because we create objects by using the createReferee() method
	private Referee() {
		
	}
	
	
	/**
	 * validates a certain area if it is allowed to put a stone on it
	 * 
	 * @param x
	 * @param y
	 * @param stone
	 * @return boolean value if the area is valid for a stone in a certain area
	 */
	public boolean isValidArea(int x, int y, Stone stone) {
		if(inPlayerArea(x, y)){
			return !getWorld().hasCollision(x,y,stone);
		}else{
			return !getWorld().hasCollision(x,y,stone) 
					&& getWorld().existNeighbor(x,y, stone) 
					&& !getWorld().outOfBoard(x,y) 
					&& hasMatchingTerm(x, y, stone)
					&& (!stone.isResultStone() || isValidAreaForResultStone(x,y,stone));
		}
	}
	
	
	private boolean isValidAreaForResultStone(int x, int y, Stone stone) {
		return hasNoDistantNeighbor(x,y,stone) && isNotBetweenStones(x,y,stone) && hasMatchingTermForResultstone(x,y,stone);
	}


	private boolean hasMatchingTermForResultstone(int x, int y, Stone stone) {
		//result is always numeric, so we create dummy stone and check
		Stone dummyStone = new Stone("0");
		// TODO Auto-generated method stub
		return true;
	}


	private boolean isNotBetweenStones(int x, int y, Stone stone) {
		// TODO Auto-generated method stub
		// ein Stein darf nicht zwischen 2 anderen steinen liegen
		// weder horizontal noch vertikal
		// diese methode wird aber nur bei =Steinen verwendet
		return true;
	}


	private boolean hasNoDistantNeighbor(int x, int y, Stone stone) {
		boolean retVal = true;
		
		//term,=,free,STONE
		if(!getWorld().existsStone(x+1, y, stone)
				&& getWorld().existsStone(x+2, y, stone)
				&& getWorld().existsStone(x-1, y, stone)){
			return false;
		}
		
		
		//STONE,free,=,term
		if(!getWorld().existsStone(x-1, y, stone)
				&& getWorld().existsStone(x-2, y, stone)
				&& getWorld().existsStone(x+1, y, stone)){
			return false;
		}
		//term
		//=
		//free
		//STONE
		if(!getWorld().existsStone(x, y+1, stone)
				&& getWorld().existsStone(x, y+2, stone)
				&& getWorld().existsStone(x, y-1, stone)){
			return false;
		}
		
		
		//STONE
		//free
		//=
		//term
		if(!getWorld().existsStone(x, y-1, stone)
				&& getWorld().existsStone(x, y-2, stone)
				&& getWorld().existsStone(x, y+1, stone)){
			return false;
		}
		
		return retVal;
	}


	private boolean inPlayerArea(int x, int y) {
    	//TODO: werte aus config verwenden
		return x > 4 && x < 12 && y == 17;
	}


	
	/**
	 * The public method "hasMatchingTerm" verifies which stone
	 * is allowed to put on which position on the term.
	 * 
	 * @param x
	 * @param y
	 * @param stone
	 * @return
	 */
	public boolean hasMatchingTerm(int x, int y, Stone stone){
		
		boolean allowedFromWest = true;
		boolean allowedFromEast = true;
		boolean allowedFromNorth = true;
		boolean allowedFromSouth = true;
		
		if(getWorld().existsStone(x-1,y,stone)){
			Stone westNeighbour = getWorld().getWestNeighbor(x, y);
			allowedFromWest = westNeighbour.getHorizontalTerm().allowedToAddAtBack(stone);
		}
		
		if(getWorld().existsStone(x+1,y,stone)){
			Stone eastNeighbour = getWorld().getEastNeighbor(x, y);
			allowedFromEast = eastNeighbour.getHorizontalTerm().allowedToAddInFront(stone);
		}

		if(getWorld().existsStone(x,y-1,stone)){
			Stone northNeighbour = getWorld().getNorthNeighbor(x, y);
			allowedFromNorth = northNeighbour.getVertikalTerm().allowedToAddAtBack(stone);
		}

		if(getWorld().existsStone(x,y+1,stone)){
			Stone southNeighbour = getWorld().getSouthNeighbor(x, y);
			allowedFromSouth = southNeighbour.getVertikalTerm().allowedToAddInFront(stone);
		}
		return allowedFromWest && allowedFromEast && allowedFromNorth && allowedFromSouth;
	}
	
	
	/**
	 * The method "showValidCells" highlights the area around the stones 
	 * to show the user where he/she can puts the next stone.
	 * 
	 * @param relocatableActor
	 */
	public void showValidCells(RelocatableActor relocatableActor) {
		if(currentValidCells.isEmpty()){
			
			List<Cell> allCells = getWorld().getCells();
			
			for(Cell c : allCells){
				int x = c.x;
				int y = c.y;
				
				if(isValidArea(x, y, (Stone) relocatableActor)){
					Highlighter h = new Highlighter();
					getWorld().addObject(h, x, y);
					currentValidCells.add(h);
				}
			}
		}
	}
	
	
	/**
	 * Removes the highlights after the move is done.
	 */
	public void hideValidCells() {
		getWorld().removeObjects(currentValidCells);
		currentValidCells.clear();
		
	}
	
	/**
	 * Validates if the game is over
	 */
	public void evalGameEnd() {
		if(getWorld().getCurrentPlayer().getStones().isEmpty()){
			getWorld().finishGame();
		}
	}

	
	private ScrabblematicsWorld getWorld(){
		return ScrabblematicsWorld.getMyWorld();
	}
}
