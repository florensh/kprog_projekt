import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.MouseInfo;

public abstract class RelocatableActor extends Actor {

    /**
     * This method is called whenever the 'Act' or 'Run' button gets pressed in the environment.
     * The method is basically for the relocation of the Stone itself!
     */
    public void act() 
    {
    	relocate();
    }
    
    
    /**
     * The method detects if the stone is dragged by the user and sets the
     * new location according to the current mouse position if there is no collision to an other stone.
     */
    private void relocate() {
    	if(Greenfoot.mouseDragged(this)){
    		if(isRelocatable()){
    			doOnDrag();
    			MouseInfo mi = Greenfoot.getMouseInfo();
    			if(isInValidArea(mi.getX(), mi.getY())){
    				setLocation(mi.getX(), mi.getY());
    			}
    		}
    	}if(Greenfoot.mouseDragEnded(this)){
    		doOnDragEnded();
    		if(isValidMove()){
    			doAfterMove();
    		}
    	}
	}

	protected abstract boolean isRelocatable();
	
	protected abstract void doOnDragEnded();
	
	protected abstract void doOnDrag();

	protected abstract boolean isInValidArea(int x, int y);
	
	protected abstract boolean isValidMove();
	
	protected abstract void doAfterMove();

}