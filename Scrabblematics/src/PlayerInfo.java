import java.awt.Color;
import java.util.List;

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class PlayerInfo here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PlayerInfo extends Actor
{
	
	private int displayWidth;	// the displays width
	private int displayHeight; // the displays height
	
	private GreenfootImage displayImage; // the Display image
    private static PlayerInfo theDisplay; // the Display object

    
    /**
     * create a display to show information
     * 
     * @param message the shown message 
     * @param width the width of the display
     * @param height the height of the display
     */
    public PlayerInfo(int width, int height  ) {
    	    	
    	displayWidth = width;
    	displayHeight = height;
    	displayImage = new GreenfootImage(displayWidth ,displayHeight);
    	
        theDisplay = this;
       
    }     
    
    
    /**
     * get the display object
     * 
     * @return the display object
     */
    public static PlayerInfo getDisplay(){
        
        return theDisplay;
    }
    
    /**
     * update the display's message
     * 
     * @param message the new message that should be shown
     */
    public void updateDisplay(List<Player> players){
      
        //clear display
    	this.displayImage.clear();
    	StringBuffer b = new StringBuffer();
    	for(int i = 0; i<players.size();i++){
    		b.append(players.get(i).getName());
    		b.append(": ");
    		b.append(players.get(i).getPoints());
    		b.append(" ");
    	}
    	
    	this.displayImage.drawImage(new GreenfootImage(b.toString(), 15, Color.GRAY, null), 0, 0);
//    	this.displayImage.drawImage(new GreenfootImage("Hallo", 15, Color.GRAY, null), 1, 1);
        this.setImage(this.displayImage);
        
    }

	
	
    /**
     * Act - do whatever the PlayerInfo wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
