import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Class to manage terms.
 * @author Florens Hueckstaedt
 *
 */
public class TermManager {
	
	private enum TermDirection{
		NORTH,SOUTH,EAST,WEST;
	}
	
	ScrabblematicsWorld world;
	
	
	/**
	 * Constructor
	 * @param world
	 */
	public TermManager(ScrabblematicsWorld world) {
		this.world = world;
	}
	
	
	
	/**
	 * Adds Stones to terms
	 * @param stone
	 */
	public void manageTerms(Stone stone){
		manageTerms(stone, true, true);
	}
	
	
	/**
	 * Adds Stones to terms
	 * @param stone
	 */
	public void manageTerms(Stone stone, boolean horizontal, boolean vertical){
		addToTerms(stone);
		
		if(stone.isResultStone()){
			finishTerms(stone);
		}
		
		

	}
	
	private void addToTerms(Stone stone) {
		int x = stone.getX();
		int y = stone.getY();
		
		Stone westNeighbor = null;
		Stone eastNeighbor = null;
		Stone northNeighbor = null;
		Stone southNeighbor = null;
		
		
		
		//resolve the neighbors
		if(!world.getObjectsAt(x-1, y, Stone.class).isEmpty()){
			westNeighbor = (Stone) world.getObjectsAt(x-1, y, Stone.class).get(0);
		}
		
		if(!world.getObjectsAt(x+1, y, Stone.class).isEmpty()){
			eastNeighbor = (Stone) world.getObjectsAt(x+1, y, Stone.class).get(0);
		}
		
		if(!world.getObjectsAt(x,y-1, Stone.class).isEmpty()){
			northNeighbor = (Stone) world.getObjectsAt(x,y-1, Stone.class).get(0);
		}
		
		if(!world.getObjectsAt(x,y+1, Stone.class).isEmpty()){
			southNeighbor = (Stone) world.getObjectsAt(x,y+1, Stone.class).get(0);
		}



		//resolve the scenario and process
		if(eastNeighbor==null && westNeighbor==null && northNeighbor==null && southNeighbor==null){
				createNewHorizontalTerm(stone);
				createNewVerticalTerm(stone);
				
		}else if(eastNeighbor==null && westNeighbor==null && northNeighbor==null && southNeighbor!=null){
				createNewHorizontalTerm(stone);
				addToExistingSouthernTerm(stone, southNeighbor);
				
		}else if(eastNeighbor==null && westNeighbor==null && northNeighbor!=null && southNeighbor==null){
				createNewHorizontalTerm(stone);
				addToExistingNorthernTerm(stone, northNeighbor);
		
		}else if(eastNeighbor==null && westNeighbor==null && northNeighbor!=null && southNeighbor!=null){
				createNewHorizontalTerm(stone);
				mergeVerticalTerms(stone, northNeighbor, southNeighbor);
		
		}else if(eastNeighbor==null && westNeighbor!=null && northNeighbor==null && southNeighbor==null){
				createNewVerticalTerm(stone);
				addToExistingWesternTerm(stone, westNeighbor);
		
		}else if(eastNeighbor==null && westNeighbor!=null && northNeighbor==null && southNeighbor!=null){
				addToExistingWesternTerm(stone, westNeighbor);
				addToExistingSouthernTerm(stone, southNeighbor);
		
		}else if(eastNeighbor==null && westNeighbor!=null && northNeighbor!=null && southNeighbor==null){
				addToExistingWesternTerm(stone, westNeighbor);
				addToExistingNorthernTerm(stone, northNeighbor);
		
		}else if(eastNeighbor==null && westNeighbor!=null && northNeighbor!=null && southNeighbor!=null){
				addToExistingWesternTerm(stone, westNeighbor);
				mergeVerticalTerms(stone, northNeighbor, southNeighbor);
		
		}else if(eastNeighbor!=null && westNeighbor==null && northNeighbor==null && southNeighbor==null){
				addToExistingEasternTerm(stone, eastNeighbor);
				createNewVerticalTerm(stone);
		
		}else if(eastNeighbor!=null && westNeighbor==null && northNeighbor==null && southNeighbor!=null){
				addToExistingEasternTerm(stone, eastNeighbor);
				addToExistingSouthernTerm(stone, southNeighbor);
		
		}else if(eastNeighbor!=null && westNeighbor==null && northNeighbor!=null && southNeighbor==null){
				addToExistingEasternTerm(stone, eastNeighbor);
				addToExistingNorthernTerm(stone, northNeighbor);
		
		}else if(eastNeighbor!=null && westNeighbor==null && northNeighbor!=null && southNeighbor!=null){
				addToExistingEasternTerm(stone, eastNeighbor);
				mergeVerticalTerms(stone, northNeighbor, southNeighbor);
		
		}else if(eastNeighbor!=null && westNeighbor!=null && northNeighbor==null && southNeighbor==null){
				createNewVerticalTerm(stone);
				mergeHorizontalTerms(stone, westNeighbor, eastNeighbor);
		
		}else if(eastNeighbor!=null && westNeighbor!=null && northNeighbor==null && southNeighbor!=null){
				addToExistingSouthernTerm(stone, southNeighbor);
				mergeHorizontalTerms(stone, westNeighbor, eastNeighbor);
		
		}else if(eastNeighbor!=null && westNeighbor!=null && northNeighbor!=null && southNeighbor==null){
				addToExistingNorthernTerm(stone, northNeighbor);
				mergeHorizontalTerms(stone, westNeighbor, eastNeighbor);
		
		}else if(eastNeighbor!=null && westNeighbor!=null && northNeighbor!=null && southNeighbor!=null){
				mergeVerticalTerms(stone, northNeighbor, southNeighbor);
				mergeHorizontalTerms(stone, westNeighbor, eastNeighbor);
		}
	}



	private void finishTerms(Stone stone) {
		
		if(ScrabblematicsWorld.getMyWorld().existsStone(stone.getX(), stone.getY()+1, stone)
				&& !ScrabblematicsWorld.getMyWorld().existsStone(stone.getX(), stone.getY()-1, stone)){
			calculateAndCreateResultStone(stone, TermDirection.SOUTH);
		}
		
		if(ScrabblematicsWorld.getMyWorld().existsStone(stone.getX(), stone.getY()-1, stone)
				&& !ScrabblematicsWorld.getMyWorld().existsStone(stone.getX(), stone.getY()+1, stone)){
			calculateAndCreateResultStone(stone, TermDirection.NORTH);
		}
		
		if(ScrabblematicsWorld.getMyWorld().existsStone(stone.getX()+1, stone.getY(), stone)
				&& !ScrabblematicsWorld.getMyWorld().existsStone(stone.getX()-1, stone.getY(), stone)){
			calculateAndCreateResultStone(stone, TermDirection.WEST);
		}
		
		
		if(ScrabblematicsWorld.getMyWorld().existsStone(stone.getX()-1, stone.getY(), stone)
				&& !ScrabblematicsWorld.getMyWorld().existsStone(stone.getX()+1, stone.getY(), stone)){
			calculateAndCreateResultStone(stone, TermDirection.EAST);
		}
	}



	private void calculateAndCreateResultStone(Stone stone, TermDirection direction) {
		String result = "";
		List<String> letterings = new ArrayList<String>();
		Stone resultStone;
		
		switch (direction) {
		
		case NORTH:
			letterings = new ArrayList<String>();
			addNeighbors(letterings, stone, 0, -1);
			Collections.reverse(letterings);
			
			result = String.valueOf(Calculator.calculate(letterings));
			resultStone = createResultStone(result);
			ScrabblematicsWorld.getMyWorld().addObject(resultStone, stone.getX(), stone.getY()+1);
			resultStone.setVertikalTerm(new Term());
			resultStone.getVertikalTerm().forwardState = Term.END;
			createNewHorizontalTerm(resultStone);
			break;
			
		case SOUTH:
			letterings = new ArrayList<String>();
			addNeighbors(letterings, stone, 0, +1);
			Collections.reverse(letterings);
			
			result = String.valueOf(Calculator.calculate(letterings));
			resultStone = createResultStone(result);
			ScrabblematicsWorld.getMyWorld().addObject(resultStone, stone.getX(), stone.getY()-1);
			resultStone.setVertikalTerm(new Term());
			resultStone.getVertikalTerm().forwardState = Term.END;
			createNewHorizontalTerm(resultStone);
			break;
			
		case EAST:
			
			letterings = new ArrayList<String>();
			addNeighbors(letterings, stone, -1, 0);
			Collections.reverse(letterings);
			
			result = String.valueOf(Calculator.calculate(letterings));
			resultStone = createResultStone(result);
			ScrabblematicsWorld.getMyWorld().addObject(resultStone, stone.getX()+1, stone.getY());
			resultStone.setHorizontalTerm(new Term());
			resultStone.getHorizontalTerm().forwardState = Term.END;
			createNewVerticalTerm(resultStone);
			break;
			
		case WEST:
			
			letterings = new ArrayList<String>();
			addNeighbors(letterings, stone, 1, 0);
			
			result = String.valueOf(Calculator.calculate(letterings));
			resultStone = createResultStone(result);
			ScrabblematicsWorld.getMyWorld().addObject(resultStone, stone.getX()-1, stone.getY());
			resultStone.setHorizontalTerm(new Term());
			resultStone.getHorizontalTerm().backwardState = Term.END;
			createNewVerticalTerm(resultStone);
			break;

		default:
			break;
		}
		
		
		//add points to the player
		stone.getPlayer().setPoints(stone.getPlayer().getPoints()+Integer.parseInt(result));
	}



	private void addNeighbors(List<String> letterings, Stone stone, int distanceTox, int distanceToy) {
		
		//recursive method
		resolveLetteringOfNeighbor(letterings, stone, distanceTox, distanceToy);
		
	}



	private Stone createResultStone(String lettering) {
		Stone resultStone = new Stone(lettering);
		return resultStone;
	}



	private void resolveLetteringOfNeighbor(List<String> letterings, Stone stone, int distanceTox, int distanceToy) {

		int xCoordinateToCheck = stone.getX()+distanceTox;
		int yCoordinateToCheck = stone.getY()+distanceToy;
		
		//exit the method if no neighbor exists
		if(!ScrabblematicsWorld.getMyWorld().existsStone(xCoordinateToCheck, yCoordinateToCheck, stone)){
			return;
		}
		
		//resolve the neighbor
		Stone neighbor = (Stone) ScrabblematicsWorld.getMyWorld().getObjectsAt(xCoordinateToCheck, yCoordinateToCheck, Stone.class).iterator().next();
		letterings.add(neighbor.getLettering());
		
		//recursive call
		resolveLetteringOfNeighbor(letterings, neighbor, distanceTox, distanceToy);
	}



	private void createNewVerticalTerm(Stone stone){
		Term t = new Term();
		stone.setVertikalTerm(t);
		t.addStoneToTheFront(stone);
		t.addStoneToTheEnd(stone);
	}
	
	private void createNewHorizontalTerm(Stone stone){
		Term t = new Term();
		stone.setHorizontalTerm(t);
		t.addStoneToTheFront(stone);
		t.addStoneToTheEnd(stone);
	}
	
	private void addToExistingEasternTerm(Stone stone, Stone eastNeighbour){
		eastNeighbour.getHorizontalTerm().addStoneToTheFront(stone);
		stone.setHorizontalTerm(eastNeighbour.getHorizontalTerm());
	}
	

	private void addToExistingWesternTerm(Stone stone, Stone westNeighbour){
		westNeighbour.getHorizontalTerm().addStoneToTheEnd(stone);
		stone.setHorizontalTerm(westNeighbour.getHorizontalTerm());
	}
	
	private void addToExistingNorthernTerm(Stone stone, Stone northNeighbour){
		northNeighbour.getVertikalTerm().addStoneToTheEnd(stone);
		stone.setVertikalTerm(northNeighbour.getVertikalTerm());
	}
	
	private void addToExistingSouthernTerm(Stone stone, Stone southNeighbour){
		southNeighbour.getVertikalTerm().addStoneToTheFront(stone);
		stone.setVertikalTerm(southNeighbour.getVertikalTerm());
	}
	
	private void mergeHorizontalTerms(Stone stone, Stone westNeighbour, Stone eastNeighbour){
		State newBackStateOfMergedTerm = westNeighbour.getHorizontalTerm().backwardState;
		westNeighbour.setHorizontalTerm(eastNeighbour.getHorizontalTerm());
		addToExistingEasternTerm(stone, eastNeighbour);
		for(Stone s : westNeighbour.getHorizontalTerm().getStones()){
			addToExistingEasternTerm(s, eastNeighbour);
		}
		westNeighbour.getHorizontalTerm().backwardState = newBackStateOfMergedTerm;
	}
	
	private void mergeVerticalTerms(Stone stone, Stone northNeighbour, Stone southNeighbour){
		State newBackStateOfMergedTerm = northNeighbour.getVertikalTerm().backwardState;
		northNeighbour.setVertikalTerm(southNeighbour.getVertikalTerm());
		addToExistingEasternTerm(stone, southNeighbour);
		for(Stone s : northNeighbour.getVertikalTerm().getStones()){
			addToExistingEasternTerm(s, southNeighbour);
		}
		northNeighbour.getVertikalTerm().backwardState = newBackStateOfMergedTerm;
	}
	
}


class Calculator{
	
    public static boolean isOperator(char c){
        switch(c) {
            case '+': return true;
            case '-': return true;
            case '*': return true;
            case '/': return true;
            default: return false;
        }
    }

	public static int calculate(List<String> letterings){
		
		int result = 0;
		
		
		
		//merge digits (1 and 0 = 10
		String tempString = "";
		List<String> mergedDigits = new ArrayList<String>();
		for(String s : letterings){
			if(!isOperator(s.charAt(0))){
				tempString = tempString+s;
			}else{
				if(!tempString.isEmpty()){
					mergedDigits.add(tempString);
					tempString = "";
					mergedDigits.add(s);
				}else{
					tempString = s;
				}
			}
		}
		
		
		if(!tempString.isEmpty()){
			mergedDigits.add(tempString);
		}

		
		//at the moment only '+' is an allowed operator
		for(String s : mergedDigits){
			if(!isOperator(s.charAt(0))){
				result = result + Integer.parseInt(s);
			}
		}
		return result;
	}
}
