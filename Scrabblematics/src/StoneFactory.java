import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import logging.ScrabblematicsLogger;
import xml.XMLReader;
import xml.XMLReader.XMLReaderException;

/**  
 * This class read via the "Configuration" getter and the "ConfigurationFactory" the values from
 * the XML file "configuration.xml for 
 * (numberOfOperandStones, numberOfOperationStones,numberOfEqualSignStones).
 * 
 * 
 * @author (Patrick Wohlgemuth)
 * @version (12.12.2013)
 *
 */
public class StoneFactory {
	
	

	/**
	 * This method reads Values from the stone.xml
	 * and creats a stone scenario
	 * @return (Hashtable)"stones"
	 */
	public static List<LocalizedStone> createStonesForBoard(){
		XMLReader readerStones;
		List<LocalizedStone> savedStones = null;

		try {

			readerStones = new XMLReader(ScrabblematicsWorld.getConfiguration().getXmlStoneDataFile());
			String[] tags = {"lettering","xValue","yValue"};

			Hashtable<String,Vector<String>> stones = readerStones.readByTags(tags);
			savedStones = new ArrayList<LocalizedStone>();

			for (int i = 0; i < stones.get("lettering").size(); i++) {

				String lettering = stones.get("lettering").get(i);
				String bereinigtLettering = lettering.trim();

				String xValue = stones.get("xValue").get(i);
				String bereinigtXValue = xValue.trim();

				String yValue = stones.get("yValue").get(i);
				String bereinigtYValue = yValue.trim();
				
				savedStones.add(new LocalizedStone(new Stone(bereinigtLettering), Integer.parseInt(bereinigtXValue), Integer.parseInt(bereinigtYValue)));


			}


		}catch (XMLReaderException e) {
			ScrabblematicsLogger.log(e);
			
		}

		return savedStones;
		
	}

	

	private static String createRandomNumbers(){
    	
    	Random random = new Random();
    	int	number = random.nextInt(9)+1;
		return String.valueOf(number);
    }
	/**
	 *	In this version is this method is unimportant, because 
	 *	the symbol ("-") is not in integrated.
	 *
	 *	@return the symbol for the operationStone (+,-)
	 */
    private static String createRandomOperators(){
    	
    	Random random = new Random();
    	String Operator;
    	int c = random.nextInt(2);
    		
    		if (c == 0){
    			 return Operator = "+";}
    		else{
    			return Operator = "-";}
    }		
   

    /**
     *  Read from the XML file the tags (numberOfOperandStones, numberOfOperationStones,numberOfEqualSignStones)
     *  by using the getter of class "Configuration", and set the number of the stones.
     *  (operatorStone,OperandStone,equalsSignStone)
     */
	public static void createStonesForPlayer(Player aktuellerSpieler){
		
    	
		for(int i = 0; i<ScrabblematicsWorld.getConfiguration().getNumberOfOperandStones();i++){
    		
    		String c = createRandomNumbers();
    		aktuellerSpieler.addStone(new Stone(c));
    	}
		
    	for(int i = 0; i<ScrabblematicsWorld.getConfiguration().getNumberOfOperationStones();i++){
    		aktuellerSpieler.addStone(new Stone(ScrabblematicsWorld.getMyWorld().getConfiguration().getDefaultPlusSign()));
    	
    		
    		
    		//Kann wieder eingef�hrt werden, wenn man den Minusoperator wieder einf�hren m�chte.
    		//String c = createRandomOperators();
        	//aktuellerSpieler.addStone(new Stone(c));
    	}
    	
        for(int i = 0; i<ScrabblematicsWorld.getConfiguration().getNumberOfEqualSignStones();i++){
            aktuellerSpieler.addStone(new Stone(ScrabblematicsWorld.getMyWorld().getConfiguration().getDefaultEqualitySign()));
    	
    	}
	}
}
