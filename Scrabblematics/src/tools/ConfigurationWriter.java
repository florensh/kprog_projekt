package tools;

import java.lang.reflect.Field;
import java.util.Hashtable;

import xml.XMLWriter;
import configuration.Configuration;

public class ConfigurationWriter {
	

	public static void main(String[] args) {
		
		String PATH = "E:\\privat\\Studium\\kprog\\git\\KPROG_Projekt\\Scrabblematics\\src\\xmldata\\empty_configuration.xml";
		String ROOTELEMENT = "configuration";
		String ELEMENTSNAME = "data";
		

		XMLWriter xmlWriter = new XMLWriter(PATH, ROOTELEMENT, ELEMENTSNAME);
		

		Hashtable<String, String> info = new Hashtable<String, String>();
		Field[] fields = Configuration.class.getDeclaredFields();
		for(Field field : fields){
			info.put(field.getName(), "");
			
		}
		
		xmlWriter.write(info);
		xmlWriter.close();
	}

}
