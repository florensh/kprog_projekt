import greenfoot.Greenfoot;
import greenfoot.GreenfootImage;
import greenfoot.MouseInfo;
import greenfoot.World;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import logging.ScrabblematicsLogger;
import configuration.Configuration;
import configuration.ConfigurationFactory;

/**
 * This is the world of the game.
 * 
 * @author (Florens Hueckstaedt)
 * @author (Patrick Wohlgemuth)
 * @author (Florian Wohlgemuth)
 * @author (Mustafa Uezuemcue)
 * @version (06.12.2013)
 */
public class ScrabblematicsWorld extends World
{
	private static ScrabblematicsWorld theWorld;
	private static Configuration configuration;
	
	private PlayerList players;
	private Player currentPlayer;
	private Stone currentMovedStone;
	private PlayerStoneStore playerStoneStore;
	private Iterator<Player> playerIterator;
	private List<Cell> cells;
	private TermManager termManager;
	private PlayerInfo playerInfo;
	private Referee referee;
	private GameSaver saver;
	
	public Referee getReferee() {
		return referee;
	}

	public List<Cell> getCells() {
		if(cells == null || cells.isEmpty()){
			cells = resolveAllCells();
		}
		return cells;
	}

	/**
	 * 
	 * @return the current Stone which was moved.
	 */
    public Stone getCurrentMovedStone() {
		return currentMovedStone;
	}

    /**
     * Set the position of the current moved stone.
     * 
     * @param currentMovedStone
     */
	public void setCurrentMovedStone(Stone currentMovedStone) {
		this.currentMovedStone = currentMovedStone;
	}

	/**
	 * 
	 * @return the current player. 
	 */
	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	/**
	 * Set the player who will do the next move.
	 * 
	 * @param currentPlayer
	 */
	public void setCurrentPlayer(Player currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	/**
	 * Constructor
	 */
	public ScrabblematicsWorld(){
        super(getConfiguration().getWorldDimensionXValue(), getConfiguration().getWorldDimensionYValue(), getConfiguration().getCellSize());
	}
	
	
	private GameSaver getSaver(){
		if(saver ==null){
			
			try {
				//Dynamic creation of an object which implements GameSaver
				Class<GameSaver> sa = (Class<GameSaver>) Class.forName(getConfiguration().getDefaultGameSaver());
				saver =  sa.newInstance();
			} catch (Exception e) {
				ScrabblematicsLogger.log(e);
			}
		}
		return saver;
	}
	
	
	
	/**
     *  Constructor.
     * 
     */
    public ScrabblematicsWorld(List<Player> player)
    {    
    	
        super(getConfiguration().getWorldDimensionXValue(), getConfiguration().getWorldDimensionYValue(), getConfiguration().getCellSize()); 
        this.playerStoneStore = new PlayerStoneStore();
        this.players = new PlayerList();
        this.termManager = new TermManager(this);
        initialize();
        
        
        players.addAll(player);
        createGUI();
        theWorld = this;
        

        for(LocalizedStone st : StoneFactory.createStonesForBoard()){
        	addObject(st.getStone(), st.getX(), st.getY());
        	termManager.manageTerms(st.getStone());
        }
        
        this.playerIterator = players.iterator();
        
        
        for(Player p : player){
        	StoneFactory.createStonesForPlayer(p);
        }
        
        setCurrentPlayer(this.playerIterator.next());
        populatePlayerAreaWithStones();

    }
    
    
    
    
    
    private void initialize() {
    	this.referee = Referee.createReferee();
		
	}

	public ScrabblematicsWorld(SaveGame sg) {
        super(getConfiguration().getWorldDimensionXValue(), getConfiguration().getWorldDimensionYValue(), getConfiguration().getCellSize()); 
        this.playerStoneStore = new PlayerStoneStore();
        this.players = new PlayerList();
        this.termManager = new TermManager(this);
        players.addAll(sg.getPlayers());
        createGUI();
        theWorld = this;
        initialize();
        
        this.playerIterator = players.iterator();
        
        setCurrentPlayer(this.playerIterator.next());
        populatePlayerAreaWithStones();
        
        for(LocalizedStone s : sg.getStones()){
        	addObject(s.getStone(), s.getX(), s.getY());
        }
        
        List<Stone> allStones = getObjects(Stone.class);
        for(Stone s : allStones){
        	s.redrawLettering();
        }
        
        
        for(int i = 0; i<players.size();i++){
        	for(Stone s : players.get(i).getStones()){
        		s.redrawLettering();
        	}
        }
	}

	private void createScoreCount(){
    	
    	int worldxValue = getConfiguration().getWorldDimensionXValue();
    	int boyrdxValue = getConfiguration().getBoardDimensionXValue();
    	
    	int x = (worldxValue-boyrdxValue)/2+5;
    	int y = (getConfiguration().getBoardDimensionYValue()+1);
    	
    	this.playerInfo = new PlayerInfo(getConfiguration().getCellSize() * 10 , getConfiguration().getCellSize());
    	
    	addObject(playerInfo, x, y);
    }
    
    
    private void createGUI() {
        createBoardFrame();
    	createBoard();
    	createPlayerArea();
        createScoreCount();
		
	}


	private void createBoardFrame() {
    	GreenfootImage bg = getBackground();
		
    	int worldxValue = getConfiguration().getWorldDimensionXValue();
    	int worldyValue = getConfiguration().getWorldDimensionYValue();
    	int cellSize = getConfiguration().getCellSize();
    	
    	GreenfootImage emptyCell = new GreenfootImage(getConfiguration().getBoardFrameImage());
    	
    	
    	for(int j = 0; j<worldyValue; j++){
    		for(int i = 0; i<worldxValue;i++){
    			int px = i * cellSize;
    			int py = j * cellSize;
    			bg.drawImage(emptyCell, px, py);
    		}
    	}
		
	}


	private void createPlayerArea() {
    	
    	GreenfootImage bg = getBackground();
    	GreenfootImage playerAreaCell = new GreenfootImage(getConfiguration().getPlayerStoneAreaImage());
    	
    	int worldyValue = getConfiguration().getWorldDimensionYValue();
    	int worldxValue = getConfiguration().getWorldDimensionXValue();
    	int cellSize = getConfiguration().getCellSize();
    	int stoneNumber = getConfiguration().getMaxStonesOfPlayer();
    	
    	int start = (worldxValue-stoneNumber)/2;
    	int end = start + stoneNumber;
    	
    	//PlayerArea
      	for(int i = start; i<end;i++){
    		int px = i * cellSize;
			int py = (worldyValue-2) * cellSize;
        	bg.drawImage(playerAreaCell, px, py);
    	}
      	
      	
      	createControls();
		
	}

    private Greenfoot thumb = null;
    private Greenfoot save = null;
    
	private void createControls() {
    	int worldxValue = getConfiguration().getWorldDimensionXValue();
    	int stoneNumber = getConfiguration().getMaxStonesOfPlayer();
    	
		int y = (getConfiguration().getWorldDimensionYValue()-2)*getConfiguration().getCellSize();
		int x = (((worldxValue-stoneNumber)/2) + stoneNumber)*getConfiguration().getCellSize();
		
		GreenfootImage thumb = new GreenfootImage(getConfiguration().getFinishRoundButtonImage());
		getBackground().drawImage(thumb, x,y);
		
		
    	int x_2 = x+1*getConfiguration().getCellSize();
    	int y_2 = y;
		
		GreenfootImage SaveButton = new GreenfootImage(getConfiguration().getSaveButtonImage());
		getBackground().drawImage(SaveButton, x_2,y_2);
		
	}
	

	/**
	 *  The act method is called by the Greenfoot framework at 
	 *  each action step in the environment.
	 */
	public void act() {
		if(isRoundendButtonPressed()){
			finishRound();
		}
		
		if(isSavedButtonPressed()){
			
			
			List<Stone> stones = getObjects(Stone.class);
			List<LocalizedStone> savedStones = new ArrayList<LocalizedStone>();
			for(Stone s : stones){
				LocalizedStone st = new LocalizedStone(s, s.getX(), s.getY());
				savedStones.add(st);
				
			}
			SaveGame sg = new SaveGame(savedStones, players);
			getSaver().save(sg);
		}
		
		super.act();
		
	}
	private boolean isRoundendButtonPressed() {
    	int worldxValue = getConfiguration().getWorldDimensionXValue();
    	int stoneNumber = getConfiguration().getMaxStonesOfPlayer();
    	
		int y = (getConfiguration().getWorldDimensionYValue()-3);
		int x = (((worldxValue-stoneNumber)/2) + stoneNumber)-1;
		
		int y_end = y + 1;
		int x_end = x + 1;
		
		MouseInfo mi = Greenfoot.getMouseInfo();
		if(Greenfoot.mouseClicked(thumb)){
			if(mi.getX() > x && mi.getY() > y && mi.getX() <= x_end && mi.getY() <= y_end){
				return true;
			}
		}
		return false;
	}
	
	private boolean isSavedButtonPressed() {
    	int worldxValue = getConfiguration().getWorldDimensionXValue();
    	int stoneNumber = getConfiguration().getMaxStonesOfPlayer();
    	
		int y = (getConfiguration().getWorldDimensionYValue()-3);
		int x = (((worldxValue-stoneNumber)/2) + stoneNumber);
		
		int y_end = y + 1;
		int x_end = x + 1;
		
		MouseInfo mi = Greenfoot.getMouseInfo();
		if(Greenfoot.mouseClicked(save)){
			if(mi.getX() > x && mi.getY() > y && mi.getX() <= x_end && mi.getY() <= y_end){
				return true;
			}
		}
		return false;
	}


	private void finishRound() {
		if(getCurrentMovedStone() != null){
			addToTerm(getCurrentMovedStone());
			getCurrentMovedStone().getPlayer().getStones().remove(getCurrentMovedStone());
			referee.evalGameEnd();
		}
		cleanUpBoardAndPrepareForNextPlayer();
	}


	private void cleanUpBoardAndPrepareForNextPlayer() {
		
		try {
			
			if(getCurrentMovedStone()!=null){
				getCurrentMovedStone().setPlayer(null);
				setCurrentMovedStone(null);
				this.getCurrentPlayer().getStones().remove(getCurrentMovedStone());
			}
			this.playerInfo.updateDisplay(players);
			this.playerStoneStore.clear();
			setCurrentPlayer(playerIterator.next());
			this.playerStoneStore.populateStones(currentPlayer.getStones());
		
		
		} catch (Exception e) {
			ScrabblematicsLogger.log(e);
		}
		
	}


	protected void finishGame() {
		new EndFrame().setVisible(true);
	}

	private void createBoard() {
    	
    	int worldxValue = getConfiguration().getWorldDimensionXValue();
    	int boardyValue = getConfiguration().getBoardDimensionYValue();
    	int boyrdxValue = getConfiguration().getBoardDimensionXValue();
    	int cellSize = getConfiguration().getCellSize();
    	
    	GreenfootImage bg = getBackground();
    	GreenfootImage boardCell = new GreenfootImage(getConfiguration().getImageOfFreeField());
    	
    	
    	int x_start = (worldxValue-boyrdxValue)/2;
    	int x_end = x_start + boyrdxValue;

    	
    	for(int j = 0; j<boardyValue; j++){
    		for(int i = x_start; i<x_end;i++){
    			int px = i * cellSize;
    			int py = (j+1) * cellSize;
    			bg.drawImage(boardCell, px, py);
    		}
    	}
	}

	/**
	 * 
	 * @return the world of the game
	 */
	public static ScrabblematicsWorld getMyWorld(){
    	return theWorld;
    	
    }
    
	/**
	 * 
	 * Method to get access to the game configuration
	 * 
	 * @return the configuration
	 */
    public static Configuration getConfiguration(){
    	if(configuration == null){
    		configuration = ConfigurationFactory.createConfiguration();
    	}
    	return configuration;
    }
    
    /**
     * The method "addToTerm" adds new laid stones to the actual term.
     * 
     * @param stone
     */
	public void addToTerm(Stone stone) {
		termManager.manageTerms(stone);
		
	}

	private void populatePlayerAreaWithStones(){
		try {
			this.playerStoneStore.populateStones(currentPlayer.getStones());

		} catch (PlayerStoneAreaTooSmallException e) {
			ScrabblematicsLogger.log(e);
		}

	}


	class PlayerStoneStore{
		
		/**
		 * The public method "populateStones" fills the player stones area with stones.
		 * Throws an exception if there are too many stones then cells.
		 * 
		 * @param stones
		 * @throws PlayerStoneAreaTooSmallException
		 */
		public void populateStones(List<Stone> stones) throws PlayerStoneAreaTooSmallException{
			int sizeOfStore = getConfiguration().getMaxStonesOfPlayer();

			if(stones.size() > sizeOfStore){
				throw new PlayerStoneAreaTooSmallException("Too many Stones to populate given are " + stones.size() + " allowed are " + sizeOfStore);
			}

			int worldyValue = getConfiguration().getWorldDimensionYValue();
			int worldxValue = getConfiguration().getWorldDimensionXValue();

			int y = worldyValue-2;
			int x = (worldxValue-sizeOfStore)/2;

			int cellCount = 0;
			for(Stone s : stones){
				addObject(s, x+cellCount, y);
				cellCount++;
			}
		}
		
		/**
		 * The public method "clear" clears the player stones area
		 */
		public void clear(){
			removeObjects(getCurrentPlayer().getStones());
		}
	}


	
	
	/**
	 * Gets an object of type Stone which is located in x+1 and y
	 * @param x
	 * @param y
	 * @return Stone
	 */
	public Stone getEastNeighbor(int x, int y){
		return (Stone) getObjectsAt(x+1, y, Stone.class).iterator().next();
		
	}
	
	
	/**
	 * Gets an object of type Stone which is located in x-1 and y
	 * @param x
	 * @param y
	 * @return Stone
	 */
	public Stone getWestNeighbor(int x, int y){
		return (Stone) getObjectsAt(x-1, y, Stone.class).iterator().next();
		
	}
	
	
	/**
	 * Gets an object of type Stone which is located in x and y-1
	 * @param x
	 * @param y
	 * @return Stone
	 */
	public Stone getNorthNeighbor(int x, int y){
		return (Stone) getObjectsAt(x,y-1, Stone.class).iterator().next();
		
	}
	
	/**
	 * Gets an object of type Stone which is located in x and y+1
	 * @param x
	 * @param y
	 * @return Stone
	 */
	public Stone getSouthNeighbor(int x, int y){
		return (Stone) getObjectsAt(x,y+1, Stone.class).iterator().next();
		
	}

	
	private List<Cell> resolveAllCells() {
		
		if(cells == null || cells.isEmpty()){
			cells = new ArrayList<Cell>();
			List<Cell> allCells = new ArrayList<Cell>();
			
			int worldxValue = getConfiguration().getWorldDimensionXValue();
			int boardyValue = getConfiguration().getBoardDimensionYValue();
			int boyrdxValue = getConfiguration().getBoardDimensionXValue();
			int cellSize = 1;
			
			
			
			int x_start = (worldxValue-boyrdxValue)/2;
			int x_end = x_start + boyrdxValue;
			
			
			for(int j = 0; j<boardyValue; j++){
				for(int i = x_start; i<x_end;i++){
					int px = i * cellSize;
					int py = (j+1) * cellSize;
					allCells.add(new Cell(px, py));
				}
			}
			 cells.addAll(allCells);
		}
		
		return cells;
	}

	
	/**
	 * @param stone stone to check for neighbors
	 * @return true if an neighbor object exist, false if not.
	 * 
	 */
	boolean existNeighbor(int x,int y, Stone stone) {
		//left,right,up,down
		return existsStone(x-1,y,stone)|| existsStone(x+1,y,stone)|| existsStone(x,y-1,stone)|| existsStone(x,y+1,stone);
	}
	


	boolean outOfBoard(int x, int y) {
    	//TODO: werte aus config verwenden
		return x < 1 || x > 15 || y < 1 || y > 15;
	}
	
	
	
    /**
     * The method detects other objects of the class stone for a certain location.
     * The location is defined by x and y coordinates as parameters.
     * @param x x-coordinate to check for collision
     * @param y y-coordinate to check for collision
     * @param stone todo
     */
	boolean hasCollision(int x, int y, Stone stone) {
		return existsStone(x, y, stone);
	} 
	
	/**
	 * The method "existsStone" determines if there is already a stone on the
	 * position where the mouse is.
	 * 
	 * @param mousePositionX
	 * @param mousePositionY
	 * @param stone tdk
	 * @return stones aren't empty.
	 */
	boolean existsStone(int mousePositionX,int mousePositionY, Stone stone){

		List<Stone> stones = getObjectsAt(mousePositionX,mousePositionY, Stone.class);
		if(!stones.isEmpty()){
			stones.remove(stone);
		}
		return !stones.isEmpty();
	}

	
	
	/**
	 * The class "EndFrame".
	 * 
	 * @author (Florian Wohlgemuth)
	 * @version (11.12.2013)
	 *
	 */
	class EndFrame extends JFrame{
		
		JLabel text;
		JLabel text2;
		JButton naechstesSpielButton;
		JButton endeButton;
		JFrame theEndFrame;
		
		/**
		 * The constructor generates the GUI EndFrame.
		 */
		public EndFrame(){
			theEndFrame = this;
			setTitle("Ende");
			setSize(500,245);
			setVisible(true);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setLocationRelativeTo(null);
			setLayout(null);
			
			text = new JLabel("Congratulations! player " + getWinner().getName() + " you win!");
			text.setBounds(80,35,500,24);
			Font schrift = (text.getFont().deriveFont(Font.BOLD + Font.ITALIC, 20));
			text.setFont(schrift);
			add(text);
			
			text2 = new JLabel("Your Score: " + getWinner().getPoints());
			text2.setBounds(120,80,400,15);
			Font schrift2 = (text2.getFont().deriveFont(Font.BOLD + Font.ITALIC,15));
			text2.setFont(schrift2);
			add(text2);
			
			naechstesSpielButton = new JButton("New Game");
			naechstesSpielButton.setBounds(30,120,200,50);
			naechstesSpielButton.addActionListener(new NextGameListener());
			add(naechstesSpielButton);
			
			endeButton = new JButton("End");
			endeButton.setBounds(250,120,200,50);
			endeButton.addActionListener(new EndListener());
			add(endeButton);
		}
		
		private Player getWinner(){
			return players.getPlayerWithMostPoints();
		}
		
		private class EndListener extends IntroWorld implements ActionListener{

			/**
			 * The public method "actionPerfomed" sets the GUI theEndFrame on false.
			 */
			public void actionPerformed(ActionEvent arg0) {
				theEndFrame.setVisible(false);

			}	
		}
		
		private class NextGameListener implements ActionListener{

			/**
			 * The public method "actionPerformed" sets the GUI theEndFrame on false and
			 * opens the IntroWorld again.
			 */
			public void actionPerformed(ActionEvent arg0) {
				theEndFrame.setVisible(false);
				Greenfoot.setWorld(new IntroWorld());
			}
		}
	}
	
}

/**
 * The class "PlayerStoneAreaTooSmallException"
 * 
 * @author (Florian Wohlgemuth)
 * @author (Florens Hueckstaedt)
 * @author (Mustafa Uezuemcue)
 * @author (Patrick Wohlgemuth)
 * @version (10.12.2013)
 */
class PlayerStoneAreaTooSmallException extends Exception{

	private String message;

	/**
	 * 
	 * @return the message
	 */
	public final String getMessage() {
		return message;
	}

	/**
	 * Constructor
	 * 
	 * @param message
	 */
	public PlayerStoneAreaTooSmallException(String message) {
		super(message);
		this.message = message;

	}
}

/**
 * The class "PlayerList" is the list which includes all of the made players
 * 
 * @author (Florian Wohlgemuth)
 * @author (Florens Hueckstaedt)
 * @author (Mustafa Uezuemcue)
 * @author (Patrick Wohlgemuth)
 * @version (10.12.2013)
 */
class PlayerList extends ArrayList<Player> implements List<Player>, Serializable{

	/**
	 * 
	 * @return the iterator
	 */
	public Iterator iterator() {
		return new InfiniteIterator();
	}
	
	public Player getPlayerWithMostPoints() {
		Player retVal = null;
		
		while(listIterator().hasNext()){
			Player p = listIterator().next();
			if(retVal == null || p.getPoints() > retVal.getPoints()){
				retVal = p;
			}
		}
		return retVal;
	}

	/**
	 * The class "InfiniteIterator" iterates in an infinite state
	 * 
	 * @author (Florian Wohlgemuth)
	 * @author (Florens HueckStaedt)
	 * @author (Mustafa Uezuemcue)
	 * @author (Patrick Wohlgemuth)
	 * @version (10.12.2013)
	 */
	class InfiniteIterator implements Iterator<Player>, Serializable{

		private transient ListIterator<Player> listItr;

		public InfiniteIterator() {
			listItr = listIterator();
		}


		@Override
		public boolean hasNext() {
			return !isEmpty();
		}

		@Override
		public Player next() {
			if(!listItr.hasNext()){
				listItr = listIterator();
			}
			return listItr.next();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();				
		}
	}
}