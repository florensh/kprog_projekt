/**
 * The class Cell. The Cells are important elements in the game
 * where the stones can be placed.
 * 
 * @author (Florens Hueckstaedt)
 * @version (08.12.2013)
 */
public class Cell {
	
	/**
	 * Constructor
	 * 
	 * @param x
	 * @param y
	 */
	public Cell(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public int x;
	public int y;

}
