import greenfoot.GreenfootImage;

import java.awt.Color;
import java.io.Serializable;

/**
 * Class of actor "Stone". A Stone is the main element in the game and has a representation at the GUI
 * 
 * @author (Florens Hueckstaedt)
 * @author (Patrick Wohlgemuth) 
 * @version (06.12.2013)
 */
public class Stone extends RelocatableActor implements Serializable
{
	
	private Player player;
	private final String lettering;
	private Term horizontalTerm;
	private Term vertikalTerm;
	
    
    protected boolean isRelocatable() {
		return isOwn() && noOtherActionDone();
	}


	private boolean noOtherActionDone() {
		if(this.equals(((ScrabblematicsWorld)getWorld()).getCurrentMovedStone())
				|| ((ScrabblematicsWorld)getWorld()).getCurrentMovedStone() == null){
			return true;
		}else{
			return false;
		}
			
	}


	protected boolean isInValidArea(int x, int y) {
		return ((ScrabblematicsWorld) getWorld()).getReferee().isValidArea(x, y, this);
	}
	
	/**
	 * 
	 * @return the horizontal term.
	 */
	public Term getHorizontalTerm() {
		return horizontalTerm;
	}

	/**
	 * Sets the horizontal term.
	 * 
	 * @param horizontalTerm
	 */
	public void setHorizontalTerm(Term horizontalTerm) {
		this.horizontalTerm = horizontalTerm;
	}

	/**
	 * 
	 * @return the vertical term.
	 */
	public Term getVertikalTerm() {
		return vertikalTerm;
	}

	/**
	 * Sets the vertical term.
	 * 
	 * @param vertikalTerm
	 */
	public void setVertikalTerm(Term vertikalTerm) {
		this.vertikalTerm = vertikalTerm;
	}


	private boolean isOwn() {
		return ((ScrabblematicsWorld)getWorld()).getCurrentPlayer().equals(getPlayer());
	}


	/**
     * Constructor for objects.
     * 
     */
	public Stone(String s){
		lettering = s;
    	drawLettering(lettering);
        
    }
    

    /**
     * The method draws a string given as parameter on the stone.
     * 
     * 
     * @param String to draw on the Stone
     * 
     */
    private void drawLettering(String s) {
    	
    	if(s.length() == 2)
    		getImage().drawImage(new GreenfootImage(s, 30, Color.BLACK, null), 6, 4);
    	else if(s.length() == 3)
    		getImage().drawImage(new GreenfootImage(s, 30, Color.BLACK, null), 0, 4);
    	else if(s.length() > 3)
    		getImage().drawImage(new GreenfootImage(s, 23, Color.BLACK, null), 0, 8);
    	else
    		getImage().drawImage(new GreenfootImage(s, 30, Color.BLACK, null), 14, 4);
    	
		
	}
    
    
    public void redrawLettering(){
    	drawLettering(getLettering());
    }
    

    /**
     * 
     * @return the player.
     */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Sets the player.
	 * 
	 * @param player
	 */
    public boolean isNumeric() {
    	return Character.isDigit(getLettering().charAt(0));
       }

    
    
    public boolean isResultStone(){
    	return this.getLettering().equals(ScrabblematicsWorld.getMyWorld().getConfiguration().getDefaultEqualitySign());
    }

	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * 
	 * @return lettering
	 */
	public String getLettering() {
		return lettering;
	}


	@Override
	protected boolean isValidMove() {
		return noOtherActionDone();
	}


	@Override
	protected void doAfterMove() {
		((ScrabblematicsWorld) getWorld()).setCurrentMovedStone(this);
		
	}


	@Override
	protected void doOnDragEnded() {
		((ScrabblematicsWorld) getWorld()).getReferee().hideValidCells();
		
	}


	@Override
	protected void doOnDrag() {
		((ScrabblematicsWorld) getWorld()).getReferee().showValidCells(this);
		
	}
}
