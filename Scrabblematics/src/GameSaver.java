
public interface GameSaver {
	
	public void save(SaveGame sg);
	public SaveGame load();

}
