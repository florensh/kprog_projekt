
import java.io.Serializable;
import java.util.*;

/**
 * The class "Player".
 * 
 * @author (Florens Hueckstaedt)
 * @version 08.12.2013
 *
 */
public class Player  implements Serializable{

	private List<Stone> stones = new ArrayList<Stone>();
	private final String name;
	
	
	
	/**
	 * getter of the name
	 * @return name of player
	 */
	public String getName() {
		return name;
	}

	private int points;

	/**
	 * The Constructor assigns names to the players.
	 * 
	 * @param name
	 */
	public Player(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return the points.
	 */
	public int getPoints() {
		return points;
	}

	/**
	 * Sets the points.
	 * 
	 * @param points
	 */
	public void setPoints(int points) {
		this.points = points;
	}

	/**
	 * The method "addStone" gives the player a stone.
	 * 
	 * @param stone
	 */
	public void addStone(Stone stone){
		this.stones.add(stone);
		stone.setPlayer(this);
	}
	
	/**
	 * 
	 * @return the stones which the list includes.
	 */
	public List<Stone> getStones(){
		return this.stones;
	}

}