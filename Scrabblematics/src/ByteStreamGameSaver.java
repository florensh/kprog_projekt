import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import logging.ScrabblematicsLogger;


/**
 * GameSafer persists the current state of the game
 * 
 * @author (Florens Hueckstaedt) 
 * @version (12.12.2013)
 */
public class ByteStreamGameSaver implements GameSaver{
	
	
	
    /**
     * saves the game to the default location
     * @param sg the saveGame 
     */
	public void save(SaveGame sg){
		
		try {
			FileOutputStream f = new FileOutputStream(ScrabblematicsWorld.getMyWorld().getConfiguration().getSaveGameFile());
			ObjectOutputStream out = new ObjectOutputStream(f);
			out.writeObject(sg);
			out.close();
			
		} catch (IOException e) {
			ScrabblematicsLogger.log(e);
		}
	}
	
	
	/**
	 * loads the current safed game
	 * @return the safeGame
	 */
	public SaveGame load(){
		FileInputStream f;
		SaveGame savegame = null;
		try {
			f = new FileInputStream(ScrabblematicsWorld.getMyWorld().getConfiguration().getSaveGameFile());
			ObjectInputStream in = new ObjectInputStream(f);
			savegame = (SaveGame) in.readObject();
			in.close();
			
		} catch (Exception e) {
			ScrabblematicsLogger.log(e);
		}
		
		return savegame;
	}
}

