

import java.io.Serializable;
import java.util.LinkedList;

/**
 * 
 * Mathe term as a wrapper for the stones on the game board. The Term decides if a certain 
 * stone is allowed to lay on a field by using two states in counter direction.
 * 
 * @author (Florens Hueckstaedt)
 * @author (Patrick Wohlgemuth)
 * @author (Florian Wohlgemuth)
 * @author (Mustafa Uezuemcue)
 *
 *@version (11.12.2013)
 */
public class Term implements Serializable{

	private LinkedList<Stone> stones = new LinkedList<Stone>();

	
	/**
	 * Returns the stones of the term
	 * @return stones of term
	 */
	public LinkedList<Stone> getStones() {
		return stones;
	}



	public static final State START = new StartState();
	public static final State OPERAND = new OperandState(); 
	public static final State OPERATOR = new OperatorState(); 
	public static final State END = new EndState();
	public static final State ERROR = new ErrorState();


	public State forwardState;
	public State backwardState;

	
	/**
	 * constructor of term which initializes with start state
	 */
	public Term() {
		forwardState = START;
		backwardState = START;
	}
	
	
	/**
	 * checks if a stone is allowed to be on the front side of the term
	 * @param stone to check
	 * @return boolean value which indicates if the stone is allowed to be on the
	 * front of the term
	 */
	public boolean allowedToAddInFront(Stone stone){
		return backwardState.isValid(stone, forwardState);
	}
	
	
	/**
	 * checks if a stone is allowed to be on the back side of the term
	 * @param stone
	 * @return boolean value which indicates if the stone is allowed to be on the
	 * back side of the term
	 */
	public boolean allowedToAddAtBack(Stone stone){
		return forwardState.isValid(stone, backwardState);
	}
	
	
	
	/**
	 * adds the stone to the front side of the term
	 * @param stone to add
	 */
	public void addStoneToTheFront(Stone stone){
		if(!stones.contains(stone)){
			stones.addFirst(stone);
		}
		backwardState = backwardState.work(stone);
		
		//in certain states the two states have dependencies to each other
		forwardState = forwardState.reactToCounterState(backwardState);
	}
	
	
	/**
	 * adds the stone to the end of the term
	 * @param stone to add
	 */
	public void addStoneToTheEnd(Stone stone){
		if(!stones.contains(stone)){
			stones.addLast(stone);
		}
		forwardState = forwardState.work(stone);
		
		//in certain states the two states have dependencies to each other
		backwardState = backwardState.reactToCounterState(forwardState);
	}
}


/**
 * The abstract state of a term. The state indicates the possibilities to add stones to the term
 * @author (Florens Hueckstaedt)
 * @author (Patrick Wohlgemuth)
 * @author (Florian Wohlgemuth)
 * @author (Mustafa Uezuemcue)
 *
 */
abstract class State {
	
	/**
	 * the work method is responsible for the state change
	 * @param the new stone of the term which causes a state change
	 * @return the new state
	 */
	abstract public State work(Stone stone);
	
	/**
	 * validation if a stone can be added to the term according to the current state
	 * @param stone to check if it'S possible to add
	 * @param counterstate to include for the decision if a stone can be added
	 * @return true if the stone can be added and false if not
	 */
	abstract boolean isValid(Stone stone, State counterstate);
	
	
	/**
	 * check the dependencies to the counterState and react with a state change
	 * @param counterState
	 * @return the state change
	 */
	public State reactToCounterState(State counterState){
		
		//if the counter state is END the whole term is finished
		if(counterState.equals(Term.END)){
			return Term.END;
		}else{
			return this;
		}
	}
}


/**
 * The initial state of a term
 * @author (Florens Hueckstaedt)
 * @author (Patrick Wohlgemuth)
 * @author (Florian Wohlgemuth)
 * @author (Mustafa Uezuemcue)
 *
 */
class StartState extends State implements Serializable{
	
	@Override
	public State work(Stone stone) {
		 if(stone.isNumeric()){
			 return Term.OPERAND;
		 }else if(stone.isResultStone()){
			 return Term.END;
		 } else{
			 return Term.OPERATOR;
		 }
	}

	@Override
	boolean isValid(Stone stone, State otherState) {
		return false;
	}
}


/**
 * the final state of the term
 * @author (Florens Hueckstaedt)
 * @author (Patrick Wohlgemuth)
 * @author (Florian Wohlgemuth)
 * @author (Mustafa Uezuemcue)
 *
 */
class EndState extends State implements Serializable{
	
	@Override
	public State work(Stone stone) {
		return Term.END;
	}

	@Override
	boolean isValid(Stone stone, State otherState) {
		return false;
	}
}


/**
 * the state indicates that the term has an operand at the end
 * @author (Florens Hueckstaedt)
 * @author (Patrick Wohlgemuth)
 * @author (Florian Wohlgemuth)
 * @author (Mustafa Uezuemcue)
 *
 */
class OperandState extends State implements Serializable{
	
	@Override
	public State work(Stone stone) {
		if(stone.isResultStone()){
			return Term.END;
			
		}else if(!stone.isNumeric()){
			return Term.OPERATOR;
			
		}else{
			return Term.OPERAND;
		}
	}

	@Override
	boolean isValid(Stone stone, State otherState) {
		return (!stone.isResultStone())||(stone.isResultStone() && otherState.equals(Term.OPERAND));
	}
}


/**
 * the state indicates that the term has an operator at the end
 * @author (Florens Hueckstaedt)
 * @author (Patrick Wohlgemuth)
 * @author (Florian Wohlgemuth)
 * @author (Mustafa Uezuemcue)
 *
 */
class OperatorState extends State implements Serializable{
	
	@Override
	public State work(Stone stone) {
		if(stone.isNumeric()){
			return Term.OPERAND;
		}else if (stone.isResultStone()){
			return Term.ERROR;
		}else{
			return Term.OPERATOR;
		}
	}

	@Override
	boolean isValid(Stone stone, State otherState) {
		return stone.isNumeric();
	}
}


/**
 * indicates that an invalid stone was added
 * @author (Florens Hueckstaedt)
 * @author (Patrick Wohlgemuth)
 * @author (Florian Wohlgemuth)
 * @author (Mustafa Uezuemcue)
 *
 */
class ErrorState extends State implements Serializable{

	@Override
	public State work(Stone stone) {
		return Term.ERROR;
	}

	@Override
	boolean isValid(Stone stone, State otherState) {
		return false;
	}
	
}
