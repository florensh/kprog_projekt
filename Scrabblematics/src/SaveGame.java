import java.io.Serializable;
import java.util.List;



/**
 * Wrapper class for stones and players
 * 
 * @author (Florens Hueckstaedt) 
 * @version (12.12.2013)
 */
public class SaveGame implements Serializable{


	private static final long serialVersionUID = -7738022947638679060L;
	List<LocalizedStone> stones;
	List<Player> players;
	
	
	
	/**
	 * Constructor to create a SaveGame
	 * @param savedStones
	 * @param players
	 */
	public SaveGame(List<LocalizedStone> savedStones, List<Player> players){
		this.stones = savedStones;
		this.players = players;
	}
	

	/**
	 * getter for stones
	 * @return stones
	 */
	public List<LocalizedStone> getStones() {
		return stones;
	}


	/**
	 * Setter for stones
	 * @param stones
	 */
	public void setStones(List<LocalizedStone> stones) {
		this.stones = stones;
	}


	/**
	 * Getter for players
	 * @return players
	 */
	public List<Player> getPlayers() {
		return players;
	}
	
	
	/**
	 * Setter for players
	 * @param players
	 */
	public void setPlayers(List<Player> players) {
		this.players = players;
	}
}


/**
 * @author (Florens Hueckstaedt)
 * @version (12.12.2013)
 */
class LocalizedStone implements Serializable{
	
	private Stone stone;
	private int x;
	private int y;
	

	
	/**
	 * constructor to create a LocalizedStone
	 * @param s Stone
	 * @param x x-coordinate of the game
	 * @param y y-coordinate of the game
	 */
	public LocalizedStone(Stone s, int x, int y) {
		this.stone = s;
		this.x = x;
		this.y = y;
	}


	
	/**
	 * Getter for stone
	 * @return stone
	 */
	public Stone getStone() {
		return stone;
	}

	/**
	 * Setter for stone
	 * @param stone
	 */
	public void setStone(Stone stone) {
		this.stone = stone;
	}


	
	/**
	 * Getter for x
	 * @return x-coordinate of the stone
	 */
	public int getX() {
		return x;
	}

	/**
	 * Setter for x
	 * @param x-coordinate of the stone
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Getter for y
	 * @return y-coordinate of the stone
	 */
	public int getY() {
		return y;
	}


	/**
	 * Setter for y
	 * @param y-coordinate of the stone
	 */
	public void setY(int y) {
		this.y = y;
	}
	
}